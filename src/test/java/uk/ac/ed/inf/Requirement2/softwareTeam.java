package uk.ac.ed.inf.Requirement2;
import org.junit.Test;
import static org.junit.Assert.*;

import uk.ac.ed.inf.utils.Utils_FlightPath;
/**
 * @author cuijiacheng on 30/01/2023
 */
public class softwareTeam {
    @Test
    public void RemainingStepTest(){
        String[] args1 = {"2023-01-01","https://ilp-rest.azurewebsites.net","dd"};
        App_computeRemainingStep.main(args1);
        assertTrue(Utils_FlightPath.getRemainingSteps()>=0);
        assertTrue(Utils_FlightPath.getRemainingSteps()<Utils_FlightPath.curr_best_steps);
    }
}
