package uk.ac.ed.inf.Requirement1.OrderValidatingTeam;
import org.junit.Test;
import static org.junit.Assert.*;

import uk.ac.ed.inf.eunms.OrderOutcomes;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;
import uk.ac.ed.inf.utils.Utils_Order;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author cuijiacheng on 29/01/2023
 */
public class OrderValidating {

    @Test
    public void CvvTest(){
        String cvv1 = "1111";
        String cvv2 = "2-1";
        String cvv3 = "1";
        String cvv4 = "401";
        assertTrue(!Utils_Order.cvvIsValid(cvv1));
        assertTrue(!Utils_Order.cvvIsValid(cvv2));
        assertTrue(!Utils_Order.cvvIsValid(cvv3));
        assertTrue(Utils_Order.cvvIsValid(cvv4));
    }

    @Test
    public void CardExpiryTest(){
        Utils.setDelivery_date("2023-03-20");
        assertTrue(Utils_Order.cardIsNotExpired("03/23"));
        assertTrue(!Utils_Order.cardIsNotExpired("02/21"));
        assertTrue(Utils_Order.cardIsNotExpired("09/23"));
        assertTrue(!Utils_Order.cardIsNotExpired("01/23"));
        assertTrue(Utils_Order.cardIsNotExpired("10/23"));
        assertTrue(Utils_Order.cardIsNotExpired("03/25"));
    }

    @Test
    public void CardNumberTest(){
        assertTrue(!Utils_Order.cardNumberIsValid("45"));
        assertTrue(!Utils_Order.cardNumberIsValid("122"));
        assertTrue(!Utils_Order.cardNumberIsValid("1222"));
        assertTrue(!Utils_Order.cardNumberIsValid("1222222222222222"));
        assertTrue(Utils_Order.cardNumberIsValid("4762238908839117"));
    }

    @Test
    public void orderValidationSystemTest(){
        URL url;
        try {
            url = new URL("https://ilp-rest.azurewebsites.net");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        Utils.setBaseURL(url);
        Utils.setDelivery_date("2023-03-01");
        Client.InitializeInformation();

        Utils_Order.checkOrders();
        assertTrue(Client.getOrders_instance().get(0).getOrderOutcome() == OrderOutcomes.InvalidCardNumber);
        assertTrue(Client.getOrders_instance().get(1).getOrderOutcome() == OrderOutcomes.InvalidExpiryDate);
        assertTrue(Client.getOrders_instance().get(2).getOrderOutcome() == OrderOutcomes.InvalidCvv);
        assertTrue(Client.getOrders_instance().get(3).getOrderOutcome() == OrderOutcomes.InvalidTotal);
        assertTrue(Client.getOrders_instance().get(4).getOrderOutcome() == OrderOutcomes.InvalidPizzaNotDefined);
        assertTrue(Client.getOrders_instance().get(5).getOrderOutcome() == OrderOutcomes.InvalidPizzaCount);
        assertTrue(Client.getOrders_instance().get(6).getOrderOutcome() == OrderOutcomes.InvalidPizzaCombinationMultipleSuppliers);
        assertTrue(Client.getOrders_instance().get(7).getOrderOutcome() == OrderOutcomes.ValidButNotDelivered);
    }
}
