package uk.ac.ed.inf.Requirement1;

import org.junit.Test;
import uk.ac.ed.inf.App;

import static org.junit.Assert.assertTrue;

/**
 * @author cuijiacheng on 31/01/2023
 */
public class performanceTest {

    @Test
    public void performance1() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "dd"};
        App.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 15000);
    }

    @Test
    public void performance2() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-21", "https://ilp-rest.azurewebsites.net", "dd"};
        App.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 15000);
    }


}
