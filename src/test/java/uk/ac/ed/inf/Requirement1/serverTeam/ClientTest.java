package uk.ac.ed.inf.Requirement1.serverTeam;
import org.junit.Test;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author cuijiacheng on 29/01/2023
 */
public class ClientTest {

    @Test
    /**
     * This is the implementation of the integration test for the server side.
     * This function will help to check whether the information fetched from the server is correct or not
     */
    public void testIntegrationServer(){
        URL base;
        try {
            base = new URL("https://ilp-rest.azurewebsites.net");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        Utils.setBaseURL(base);
        Utils.setDelivery_date("2023-03-01");

        //Fetch the information
        Client.InitializeInformation();

        //Check if the fetched information is correct on size
        assertEquals(Client.getOrders_instance().size(),47);
        assertEquals(Client.getCentralAreas_instance().size(),4);
        assertEquals(Client.getNoFlyZones_instance().size(),4);
        assertEquals(Client.getRestaurants_instance().size(),4);

        //check if the detailed information is correct for no-fly zone
        assertEquals(Client.getNoFlyZones_instance().get(0).getName(),"George Square Area");
        assertEquals(Client.getNoFlyZones_instance().get(1).getName(), "Dr Elsie Inglis Quadrangle");
        assertEquals(Client.getNoFlyZones_instance().get(2).getName(), "Bristo Square Open Area");
        assertEquals(Client.getNoFlyZones_instance().get(3).getName(), "Bayes Central Area");

        //check if detailed information is correct for central areas
        assertEquals(Client.getCentralAreas_instance().get(0).getName(),"Forrest Hill");
        assertEquals(Client.getCentralAreas_instance().get(1).getName(),"Top of the Meadows");
        assertEquals(Client.getCentralAreas_instance().get(2).getName(), "Buccleuch St bus stop");
        assertEquals(Client.getCentralAreas_instance().get(3).getName(),"KFC");

        //check if detailed information is correct for central areas
        assertEquals(Client.getRestaurants_instance().get(0).getName(),"Civerinos Slice");
        assertEquals(Client.getRestaurants_instance().get(1).getName(), "Sora Lella Vegan Restaurant");
        assertEquals(Client.getRestaurants_instance().get(2).getName(), "Domino's Pizza - Edinburgh - Southside");
        assertEquals(Client.getRestaurants_instance().get(3).getName(), "Sodeberg Pavillion");

    }
}
