package uk.ac.ed.inf.Requirement1;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author cuijiacheng on 30/01/2023
 */
public class StressTest {
    @Test
    //This test if the program can stop in 1min
    public void stressTest1() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-2.9349452336243758",
                "54.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest2() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "2.9349452336243758",
                "54.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest3() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-2.9349452336243758",
                "-54.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest4() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-0.9349452336243758",
                "54.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest5() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-0.9349452336243758",
                "4.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest6() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-2.9349452336243758",
                "0.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest7() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-2.9349452336243758",
                "1.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest8() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-22.9349452336243758",
                "4.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest9() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "22.9349452336243758",
                "4.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

    @Test
    //This test if the program can stop in 1min
    public void stressTest10() {
        long start = System.currentTimeMillis();
        String[] args = {"2023-03-20", "https://ilp-rest.azurewebsites.net", "False", "-22.9349452336243758",
                "44.901164386250656"};
        App_scaffolding.main(args);
        //call the web service
        long end = System.currentTimeMillis();
        long responseTime = end - start;
        assertTrue(responseTime < 60000);
    }

}