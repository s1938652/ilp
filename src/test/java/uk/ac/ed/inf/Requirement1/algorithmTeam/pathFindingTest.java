package uk.ac.ed.inf.Requirement1.algorithmTeam;
import org.junit.Test;
import uk.ac.ed.inf.Requirement1.App_scaffolding;
import uk.ac.ed.inf.classes.LngLat;
import uk.ac.ed.inf.utils.Utils_FlightPath;


public class pathFindingTest {
    @Test
    /**
     * This is the unit test to compute the heuristic value
     */
    public void HeuristicTest(){
        // We apply the heuristic function to compute the expected steps
        LngLat l11 = new LngLat(55.9445,-3.1869);
        LngLat l12 = new LngLat(55.9433,-3.2025);
        double output1 = Utils_FlightPath.Heuristic(l11,l12);
        assert output1==104.30723848324135;

        LngLat l21 = new LngLat(55.9445,-3.1869);
        LngLat l22 = new LngLat(55.9445,-3.1839);
        double output2 = Utils_FlightPath.Heuristic(l21,l22);
        assert output2==20.00000000000076;

        LngLat l31 = new LngLat(55.9445,-3.1869);
        LngLat l32= new LngLat(	55.9455,-3.1913);
        double output3 = Utils_FlightPath.Heuristic(l31,l32);
        assert output3==30.0813711271494;

        LngLat l41 = new LngLat(55.9445,-3.1869);
        LngLat l42 = new LngLat(55.9439,-3.1940);
        double output4 = Utils_FlightPath.Heuristic(l41,l42);
        assert output4==47.50204673952794;
    }

    @Test
    public void pathfindingTest(){
        //The first arg is the order date, second is the address of the server, the third is whether we have the no-fly zones or not
        //The forth and fifth is the longitude and latitude of the starting point, and the last is if we are applying stress test
        String[] args = {"2023-03-01","https://ilp-rest.azurewebsites.net","False", "-3.1878","55.9431"};
        App_scaffolding.main(args);
    }

    @Test
    public void pathFinding_pathValidationTest(){
        //The first arg is the order date, second is the address of the server, the third is whether we have the no-fly zones or not
        //The forth and fifth is the longitude and latitude of the starting point, and the last is if we are applying stress test
        String[] args = {"2023-03-01","https://ilp-rest.azurewebsites.net","True","-3.1878","55.9431"};
        App_scaffolding.main(args);
    }



}
