package uk.ac.ed.inf.Requirement1.serverTeam;

import org.junit.Test;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author cuijiacheng on 31/01/2023
 */
public class performanceServerTest {
    @Test
    public void performanceServerTest1(){
        long start_time = System.currentTimeMillis();
        URL base;
        try {
            base = new URL("https://ilp-rest.azurewebsites.net");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        Utils.setBaseURL(base);
        Utils.setDelivery_date("2023-03-01");

        //Fetch the information
        Client.InitializeInformation();
        long end_time = System.currentTimeMillis();
        assert (end_time-start_time<5000);
    }
}
