package uk.ac.ed.inf.Requirement1.algorithmTeam;
import org.junit.Test;
import static org.junit.Assert.*;
import uk.ac.ed.inf.classes.LngLat;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author cuijiacheng on 29/01/2023
 */
public class pathValidationTest {
    @Test
    public void point_in_PolygonTest(){
        double[] xs = {-3.1924646079868637,-3.19213344384454,-3.1866064284987488,-3.185738550055504,-3.188136635226641,-3.187805471084289,-3.1924646079868637};
        double[] ys = {55.94570158633812,55.94403254771126,55.943086087718115,55.94462087604907,55.944889457753675,55.946392201970696,55.94570158633812};

        //these are not inside the polygon
        LngLat p1 = new LngLat(-3.1872573373302373, 55.94514524812158);
        LngLat p2 = new LngLat(-3.188890319137414,
                55.943073297559835);
        LngLat p3 = new LngLat(-3.1927272554111994,
                55.94508769543609);
        LngLat p4 = new LngLat(-3.185464483178947,
                55.94364885050169);
        LngLat p5 = new LngLat(-3.1903634285995395,
                55.946424174618244);
        assertTrue(!LngLat.point_in_Polygon(xs,ys,p1.getLng(),p1.getLat()));
        assertTrue(!LngLat.point_in_Polygon(xs,ys,p2.getLng(),p2.getLat()));
        assertTrue(!LngLat.point_in_Polygon(xs,ys,p3.getLng(),p3.getLat()));
        assertTrue(!LngLat.point_in_Polygon(xs,ys,p4.getLng(),p4.getLat()));
        assertTrue(!LngLat.point_in_Polygon(xs,ys,p5.getLng(),p5.getLat()));

        //these are inside the polygon
        LngLat p6 = new LngLat(-3.1907859483679886,
                55.94541382618951);
        LngLat p7 = new LngLat(-3.1875770820203684,
                55.944550532913865);
        LngLat p8 = new LngLat(-3.1867891397501182,
                55.94329712472026);
        LngLat p9 = new LngLat(-3.1904662036779428,
                55.944627270873156);
        LngLat p10 = new LngLat(-3.1890730303887835,
                55.944166840836516);
        assertTrue(LngLat.point_in_Polygon(xs,ys,p6.getLng(),p6.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p7.getLng(),p7.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p8.getLng(),p8.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p9.getLng(),p9.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p10.getLng(),p10.getLat()));

        //These are the boundary test
        LngLat p11 = new LngLat(-3.1878060772699826,
                55.94639158282803);
        LngLat p12 = new LngLat(-3.189030120747873,
                55.94621066220827);
        LngLat p13 = new LngLat(-3.1924620354369893,
                55.94570067837415);
        LngLat p14 = new LngLat(-3.1918585745729047,
                55.94398622040029);
        LngLat p15 = new LngLat(-3.188982834575654,
                55.943541069307855);
        assertTrue(LngLat.point_in_Polygon(xs,ys,p11.getLng(),p11.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p12.getLng(),p12.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p13.getLng(),p13.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p14.getLng(),p14.getLat()));
        assertTrue(LngLat.point_in_Polygon(xs,ys,p15.getLng(),p15.getLat()));
    }

    @Test
    public void intersectionTest(){
        //l1 and l2 forms a line
        LngLat l1 = new LngLat(-3.142151302887413,
                55.94249904713985);
        LngLat l2 = new LngLat(-3.134398440114353,
                55.9409848624598);

        //l3 and l4 forms a line
        LngLat l3 = new LngLat(-3.13397496441695,
                55.94419559338607);
        LngLat l4 = new LngLat(-3.13788397085699,
                55.94229837546453);

        //line l1 and l2, should not intersect with line l3 and l4
        assertTrue(!LngLat.intersects(l1, l2, l3, l4));

        //line l1 and l3,shouldn't intersect line l2 and l4
        assertTrue(!LngLat.intersects(l1, l3, l2, l4));

        //line l1 and l4,shouldn't intersect line l2 and l3
        assertTrue(!LngLat.intersects(l1, l4, l2, l3));

        //Boundary test
        LngLat l5 = new LngLat(-3.1441256779224034,
                55.93828074464639);
        LngLat l6 = new LngLat(-3.137643938209294,
                55.939107444458045);
        LngLat l7 = new LngLat(-3.1383926540175935,
                55.93725034047125);
        assertTrue(LngLat.intersects(l5,l6,l6,l7));
    }

    @Test
    public void checkIfViolatesNoFlyZonesRuleTest(){
        URL url;
        try {
            url = new URL("https://ilp-rest.azurewebsites.net");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        Utils.setBaseURL(url);
        Utils.setDelivery_date("2023-03-01");
        Client.InitializeInformation();
        LngLat l1 = new LngLat(-3.18794573813571,
                55.944527357157114);
        LngLat l2 = new LngLat(-3.1875191229281654,
                55.944320307760904);
        LngLat l3 = new LngLat(-3.187500219570353,
                55.944504236072305);
        assertTrue(l1.checkIfViolatesNoFlyZonesRule(l2));
        assertTrue(!l1.checkIfViolatesNoFlyZonesRule(l3));

        LngLat l4 = new LngLat(-3.1902996513187816,
                55.94530217260308);
        LngLat l5 = new LngLat(-3.1899110333988006,
                55.9452247146113);
        LngLat l6 = new LngLat(-3.189909235269056,
                55.945439961717824);
        assertTrue(l4.checkIfViolatesNoFlyZonesRule(l5));
        assertTrue(!l4.checkIfViolatesNoFlyZonesRule(l6));

    }
}
