package uk.ac.ed.inf.Requirement1;

import org.junit.Test;
import uk.ac.ed.inf.App;

/**
 * @author cuijiacheng on 30/01/2023
 */
public class systemTest {
    @Test
    public void systemTest1(){
        String[] args = {"2023-03-01","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest2(){
        String[] args = {"2023-03-02","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest3(){
        String[] args = {"2023-03-03","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest4(){
        String[] args = {"2023-03-04","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest5(){
        String[] args = {"2023-03-05","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest6(){
        String[] args = {"2023-03-06","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest7(){
        String[] args = {"2023-03-07","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest8(){
        String[] args = {"2023-03-08","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest9(){
        String[] args = {"2023-03-09","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest10(){
        String[] args = {"2023-03-10","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest11(){
        String[] args = {"2023-03-11","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest12(){
        String[] args = {"2023-03-12","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }

    @Test
    public void systemTest13(){
        String[] args = {"2023-03-13","https://ilp-rest.azurewebsites.net","dd"};
        App.main(args);
    }
}
