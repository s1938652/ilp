package uk.ac.ed.inf;

import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;
import uk.ac.ed.inf.utils.Utils_Order;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author cuijiacheng on 30/01/2023
 */
public class script {

    /**
     * This function is used to check if a given url is in a valid URL
     * and will end the program if the input url_str is invalid
     * @param url_str a String that represents the base url
     * @return URL if the input url_str is valid
     */
    private static URL checkUrl(String url_str){
        URL base_url = null;
        try {
            base_url = new URL(url_str);
        } catch (MalformedURLException e) {
            System.err.println("The given base url is incorrect, ending the program.....");
            System.exit(0);
        }
        return base_url;
    }

    /**
     * This is the main entry of the whole project
     * @param args An array that contains three useful information<br>
     *             The first is the delivery's date<br>
     *             The second is the Url of the server<br>
     *             The third is the random variable<br>
     */
    public static void main(String [] args){
        String url_str = args[0];
        URL base_url = checkUrl(url_str);
        Utils.setBaseURL(base_url);

        //1月
        for (int i=0; i<31; i++){
            String date = "2023-01-";
            int day = i+1;
            String day_str;
            if(day<10){
                day_str = "0"+String.valueOf(day);
            }else {
                day_str = String.valueOf(day);
            }
            String final_day = date+day_str;
            Utils.setDelivery_date(final_day);
            Client.InitializeInformation();
            Utils_Order.checkOrders();
        }
        System.out.println("1月有"+Utils_Order.wrong_num);
        Utils_Order.wrong_num = 0;

        //2月
        for (int i=0; i<28; i++){
            String date = "2023-02-";
            int day = i+1;
            String day_str;
            if(day<10){
                day_str = "0"+String.valueOf(day);
            }else {
                day_str = String.valueOf(day);
            }
            String final_day = date+day_str;
            Utils.setDelivery_date(final_day);
            Client.InitializeInformation();
            Utils_Order.checkOrders();
        }
        System.out.println("2月有"+Utils_Order.wrong_num);
        Utils_Order.wrong_num = 0;

        //3月
        for (int i=0; i<31; i++){
            String date = "2023-03-";
            int day = i+1;
            String day_str;
            if(day<10){
                day_str = "0"+String.valueOf(day);
            }else {
                day_str = String.valueOf(day);
            }
            String final_day = date+day_str;
            Utils.setDelivery_date(final_day);
            Client.InitializeInformation();
            Utils_Order.checkOrders();
        }
        System.out.println("3月有"+Utils_Order.wrong_num);

        Utils_Order.wrong_num = 0;

        //4月
        for (int i=0; i<30; i++){
            String date = "2023-04-";
            int day = i+1;
            String day_str;
            if(day<10){
                day_str = "0"+String.valueOf(day);
            }else {
                day_str = String.valueOf(day);
            }
            String final_day = date+day_str;
            Utils.setDelivery_date(final_day);
            Client.InitializeInformation();
            Utils_Order.checkOrders();
        }
        System.out.println("4月有"+Utils_Order.wrong_num);

        Utils_Order.wrong_num = 0;
        //5月
        for (int i=0; i<28; i++){
            String date = "2023-05-";
            int day = i+1;
            String day_str;
            if(day<10){
                day_str = "0"+String.valueOf(day);
            }else {
                day_str = String.valueOf(day);
            }
            String final_day = date+day_str;
            Utils.setDelivery_date(final_day);
            Client.InitializeInformation();
            Utils_Order.checkOrders();
        }
        System.out.println("5月有"+Utils_Order.wrong_num);

    }
}
