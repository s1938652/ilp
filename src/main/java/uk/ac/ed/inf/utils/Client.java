package uk.ac.ed.inf.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import uk.ac.ed.inf.classes.CentralAreas;
import uk.ac.ed.inf.classes.NoFlyZone;
import uk.ac.ed.inf.classes.Order;
import uk.ac.ed.inf.classes.Restaurant;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is a static singleton class and is used to fetch information from the server and use the fetched information to initialize the private properties. <br>
 * This class has 4 static properties: orders_instance(List of Order), restaurants_instance(List of Restaurant), noFlyZones_instance(List of NoFlyZone>), centralAreas_instance(List of Central Area).
 * @author Jiacheng Cui
 */
public class Client {
    //The 4 private properties
    /**
     * This private property is used to store the information about orders from the server
     */
    private static List<Order> orders_instance = new ArrayList<>();

    /**
     * This private property is used to store the information about restaurants from the server
     */
    private static List<Restaurant> restaurants_instance = new ArrayList<>();

    /**
     * This private property is used to store the information about no-fly zones from the server
     */
    private static List<NoFlyZone> noFlyZones_instance = new ArrayList<>();

    /**
     * This private property is used to store the information about central area from the server
     */
    private static List<CentralAreas> centralAreasPoints_instance = new ArrayList<>();

    /**
     * Getter method for the private property orders_instance
     * @return the list of Order class fetched from the server
     */
    public static List<Order> getOrders_instance() {return orders_instance;}

    /**
     * Getter method for the private property restaurants_instance
     * @return the list of Restaurant class fetched from the server
     */
    public static List<Restaurant> getRestaurants_instance() {return restaurants_instance;}

    /**
     * Getter method for the private property noFlyZones_instance
     * @return the list of NoFlyZone class fetched from the server
     */
    public static List<NoFlyZone> getNoFlyZones_instance() {return noFlyZones_instance;}

    public static void setNoFlyZonesToNothing(){ noFlyZones_instance = new ArrayList<>();}
    /**
     * Getter method for the private property centralAreasPoints_instance
     * @return the list of CentralAreas class fetched from the server
     */
    public static List<CentralAreas> getCentralAreas_instance() {return centralAreasPoints_instance;}

    /**
     * This method will access to the server, gets all the orders on that delivery date and initialize orders_instance
     */
    private static void getOrdersFromServer() {
        ObjectMapper objectMapper = new ObjectMapper();
        URL newUrl = null;
        //check if the base url is correct, if it is incorrect, we should end the program
        try {
            newUrl = new URL(Utils.getBaseURL()+"/orders/"+Utils.getDelivery_date());
        } catch (MalformedURLException e) {
            System.err.println("The given base url is incorrect, ending the program.....");
            System.exit(0);
        }
        //check if we can fetch information on the server if we can't, we should end the program
        try {
            orders_instance = objectMapper.readValue(newUrl, new TypeReference<>() {});
        } catch (IOException e) {
            System.err.println("Can't fetch orders from the server, ending the program.....");
            System.exit(0);
        }
    }

    /**
     * This method will access to the server, gets all the active restaurants and initialize restaurants_instance
     */
    private static void getRestaurantsFromServer(){
        ObjectMapper objectMapper = new ObjectMapper();
        URL newUrl = null;
        //check if the base url is correct, if it is incorrect, we should end the program
        try {
            newUrl = new URL(Utils.getBaseURL()+"/restaurants");
        } catch (MalformedURLException e) {
            System.err.println("The given base url is incorrect, ending the program.....");
            System.exit(0);
        }
        //check if we can fetch information on the server if we can't, we should end the program
        try {
            restaurants_instance = objectMapper.readValue(newUrl, new TypeReference<>() {});
        } catch (IOException e) {
            System.err.println("Can't fetch restaurants from the server, ending the program.....");
            System.exit(0);
        }
    }

    /**
     * This method will access to the server, gets all the no-fly zones and initialize noFlyZones_instance
     */
    private static void getNoFlyZonesFromServer(){
        ObjectMapper objectMapper = new ObjectMapper();
        URL newUrl = null;
        //check if the base url is correct, if it is incorrect, we should end the program
        try {
            newUrl = new URL(Utils.getBaseURL()+"/noFlyZones");
        } catch (MalformedURLException e) {
            System.err.println("The given base url is incorrect, ending the program.....");
            System.exit(0);
        }
        //check if we can fetch information on the server if we can't, we should end the program
        try {
            noFlyZones_instance = objectMapper.readValue(newUrl, new TypeReference<>() {});
        } catch (IOException e) {
            System.err.println("Can't fetch no-fly zones from server, ending the program.....");
            System.exit(0);
        }
    }

    /**
     * This method will access to the server, gets all the central area information and initialize centralAreaPoints_instance
     */
    private static void getCentralAreasFromServer(){
        ObjectMapper objectMapper = new ObjectMapper();
        URL url = null;
        //check if the base url is correct, if it is incorrect, we should end the program
        try {
            url = new URL(Utils.getBaseURL() +"/centralArea");
        } catch (MalformedURLException e) {
            System.err.println("The given base url is incorrect, ending the program.....");
            System.exit(0);
        }
        //check if we can fetch information on the server if we can't, we should end the program
        try {
            centralAreasPoints_instance = objectMapper.readValue(url, new TypeReference<>() {});
        } catch (IOException e) {
            System.err.println("Can't fetch central areas from server, ending the program.....");
            System.exit(0);
        }
    }

    /**
     * This method will access the server and initialize all the instances in this class.
     * This method will end the program if the base url is invalid, or we can't fetch anything from this url+endpoint
     */
    public static void InitializeInformation(){
        //Initialize the orders
        getOrdersFromServer();
        //initial the restaurant and end the program if no restaurants are available
        getRestaurantsFromServer();
        //initialize the no-fly zones
        getNoFlyZonesFromServer();
        //initialize the central areas
        getCentralAreasFromServer();
    }

    /**
     * This function will check the initialized instances in this class. <br>
     * A 0 size no-fly zone is acceptable, but 0 size central area or orders or restaurants is not tolerant. <br>
     * We will end the program if any of the above intolerant situation occurs.
     */
    public static void checkClientInformation(){
        boolean quit_and_write_files=false;
        List<String> zero_size_part = new ArrayList<>();
        if(centralAreasPoints_instance.size()==0){
            quit_and_write_files=true;
            zero_size_part.add("centralAreas");
        }
        if(restaurants_instance.size()==0){
            quit_and_write_files=true;
            zero_size_part.add("restaurants");
        }
        if(orders_instance.size()==0){
            quit_and_write_files=true;
            zero_size_part.add("orders");
        }
        if(quit_and_write_files){
            for(int i=0; i<zero_size_part.size(); i++){
                System.err.println("System need to be terminated because we have no "+zero_size_part.get(i)+" on the server");
            }
            Utils.writeJsonFile("./resultfiles");
            System.out.println("Files has been written");
            System.exit(0);
        }
    }

}
