package uk.ac.ed.inf.utils;

import uk.ac.ed.inf.classes.Menu;
import uk.ac.ed.inf.classes.Restaurant;
import java.util.*;

/**
 * This class is a static class that stores some useful methods that can’t be included in the Restaurant class.
 * This class has 4 methods: <br>
 * computeNamePrice, whichOrderItemsInMenu, allOrderItemsInMenus, getRestaurantByName
 */
public class Utils_Restaurant {

    /**
     * This function computes the names of all the pizzas on the restaurant's menu
     * @param restaurant - represents the restaurant you want to get the menu names
     * @return a String array which describes all the pizza names appeared on menu
     */
    private static String[] computeMenuNames(Restaurant restaurant){
        Menu[] menus = restaurant.getMenus();
        String[] menu_names = new String[menus.length];
        for (int i=0; i<menus.length; i++){
            menu_names[i] = menus[i].getName();
        }
        return menu_names;
    }

    /**
     * This method helps to compute the map between the pizza's name and the price in the menus
     * @param restaurant - which restaurant you want to process
     * @return a map which takes all menu names as keys and the corresponding price as values
     */
    public static Map<String,Integer> computeNamePrice(Restaurant restaurant){
        Menu[] menus = restaurant.getMenus();
        Map<String,Integer> namePrice = new Hashtable();
        for (int i=0; i< menus.length; i++){
            namePrice.put(menus[i].getName(),menus[i].getPriceInPence());
        }
        return namePrice;
    }

    /**
     * This function computes which certain ordered item is contained in the current restaurant's menu
     * @param orderItems - represents all the ordered items
     * @param restaurant - which represent the restaurant you want to check
     * @return a list of Integer which represents the index of ordered item which occurs in the current menu
     */
    public static List<Integer> whichOrderItemsInMenus(String[] orderItems, Restaurant restaurant){
        if (orderItems==null|restaurant==null){
            throw new NullPointerException("Names can't be null");
        }
        List<Integer> orderItemPosition = new ArrayList<>();
        String[] menu_names = computeMenuNames(restaurant);
        for (int i=0; i<orderItems.length; i++){
            if (Arrays.asList(menu_names).contains(orderItems[i])){
                orderItemPosition.add(i);
            }
        }
        return orderItemPosition;
    }

    /**
     * This function is used to check if all the ordered items appears on the menu
     * @param restaurant - which represent the restaurant you want to check
     * @param orderItems - the ordered pizzas' name
     * @return True if all the ordered items appear on the menu, vice versa
     */
    public static boolean allOrderItemsInMenus(String[] orderItems, Restaurant restaurant){
        if (orderItems==null){
            throw new NullPointerException("Names can't be null");
        }
        String[] menu_names = computeMenuNames(restaurant);
        for (int i=0; i<orderItems.length; i++){
            if (!Arrays.asList(menu_names).contains(orderItems[i])){
                return false;
            }
        }
        return true;
    }

    /**
     * This function returns the specific restaurant class by giving its restaurant's name
     * @param restaurant_name represents the name of the restaurant
     * @return the Restaurant class with the restaurant name
     */
    public static Restaurant getRestaurantByName(String restaurant_name){
        for (int i = 0; i<Client.getRestaurants_instance().size(); i++){
            Restaurant curr_restaurant = Client.getRestaurants_instance().get(i);
            if (curr_restaurant.getName().equals(restaurant_name)){
                return curr_restaurant;
            }
        }
        return null;
    }

}
