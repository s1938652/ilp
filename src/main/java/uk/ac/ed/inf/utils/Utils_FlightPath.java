package uk.ac.ed.inf.utils;

import uk.ac.ed.inf.eunms.DroneDirections;
import uk.ac.ed.inf.eunms.OrderOutcomes;
import uk.ac.ed.inf.classes.*;
import java.util.*;

/**
 * This class is a static class that stores some useful methods that can’t be included in the FlightPath class. <br>
 * this class has 4 private property <br>
 * restaurants_flightPaths_map, restaurants_AvgComputingTime_map,orders_flightPaths, chosen_num_directions, maximum_time_computingAStar, maximum_time_computingRestaurantsFlightPaths <br>
 * But only orders_flightPaths can be accessed by other class. <br>
 * this class has 1 public method: <br>
 * computeFlightPaths
 */
public class Utils_FlightPath {
    /**
     * The remaining steps of a drone
     */
    private static int remainingSteps = Utils.BATTERY;

    /**
     * This private property is used to store the map of restaurant's name and
     * a list of FlightPath which indicates the routes you take from the starting point to this restaurant
     */
    private static Map<String, List<FlightPath>> restaurants_flightPaths_map = new HashMap<>();

    /**
     * This private property is used to store the map of restaurant's name and
     * an Integer which indicates the time taken to calculate the flight path to current restaurant
     */
    private static Map<String, Integer> restaurants_AvgComputingTime_map = new HashMap<>();

    /**
     * This private property is used to store the drone's flight paths to deliver orders
     */
    private static List<FlightPath> orders_flightPaths = new ArrayList<>();

    /**
     * This private property is used to store how many directions are used while computing each restaurant's flight paths using A* algorithm
     */
    private static Map<String, Integer> chosen_num_directions = new HashMap<>();

    /**
     * This private property is used to control the maximum time used to run A* algorithm for one time
     */
    private static int maximum_time_computingAStar = 10000;

    /**
     * This private property is used to control the maximum time used to compute the flight paths to each restaurant
     */
    private static long maximum_time_computingRestaurantsFlightPaths = 30000;

    /**
     * This is the Getter method for accessing the private property orders_flightPaths
     * @return the value of private property orders_flightPaths
     */
    public static List<FlightPath> getOrders_flightPaths() {return orders_flightPaths;}

    public static int getRemainingSteps() {return remainingSteps;}
    public static int curr_best_steps;

    /**
     * This method is used to compute the flight paths to each unique restaurant which takes part in the delivery.
     * This method will first compute the flight paths using 4 directions A*(since this is fast), and use this as our initial flight paths.
     * After computing 4-directions A*, then the method will compute the second time A* using either 8-directions for the long step sizes restaurant or
     * 16-directions for the short step sizes restaurant. If we can get these results within the maximum computing time, we update the flight paths.
     */
    private static void CalculatingFlightPathsToRestaurant(){
        List<Restaurant> restaurants_needed = Utils.computeUniqueRestaurantsInOrders();
        System.out.println("\nStart to processing the flight paths to each restaurants");

        long start_computing_time = System.currentTimeMillis();
        //Traverse everything inside restaurants_needed and compute the flight path using 4 directions A*
        for (int i=0; i<restaurants_needed.size(); i++){
            long start = System.currentTimeMillis();
            Restaurant curr_restaurant = restaurants_needed.get(i);
            LngLat curr_restaurant_loc = new LngLat(curr_restaurant.getLongitude(),curr_restaurant.getLatitude());
            List<FlightPath> fps = AStar(Utils.getStartPoint(),curr_restaurant_loc,4,start_computing_time);
            if(fps!=null){
                restaurants_flightPaths_map.put(curr_restaurant.getName(),fps);
                chosen_num_directions.put(curr_restaurant.getName(),4);
            }
            long end = System.currentTimeMillis();
            restaurants_AvgComputingTime_map.put(curr_restaurant.getName(), (int) (end-start));
            //if the remaining time for calculating is not enough end the calculation
            if(end-start>= maximum_time_computingRestaurantsFlightPaths){
                OrderAnalyze();
                ComputeAverageComputationTime();
                return;
            }
        }
        long end_time_computing_4_directions = System.currentTimeMillis();
        System.out.println("We have applied the 4 directions A* algorithm and it takes "+ (end_time_computing_4_directions-start_computing_time)+"ms");

        System.out.println("Applying second time A* algorithm");
        //sort the hashmap
        SortRestaurantsFlightPathsByStepSize();
        //Traverse the result of four directions A* algorithm and apply the A star for second time
        for (String key: restaurants_flightPaths_map.keySet()){
            Restaurant curr_r = Utils_Restaurant.getRestaurantByName(key);
            LngLat restaurant = new LngLat(curr_r.getLongitude(),curr_r.getLatitude());
            long cur_time = System.currentTimeMillis();
            //end the calculation if we are running out of time
            if(cur_time-start_computing_time>= maximum_time_computingRestaurantsFlightPaths){
                OrderAnalyze();
                ComputeAverageComputationTime();
                return;
            }
            //This is the case when the step size is too far from the start point, we can only use 8 directions for computing the A star second time
            if(restaurants_flightPaths_map.get(key).size()>120){
                //This is the flight paths calculated by A star using 8 directions
                List<FlightPath> fps = AStar(Utils.getStartPoint(),restaurant,8,start_computing_time);
                //set the value if it is not null
                if(fps!=null){
                    restaurants_flightPaths_map.replace(key,fps);
                    chosen_num_directions.replace(curr_r.getName(),8);
                }
            }else{
                List<FlightPath> fps = AStar(Utils.getStartPoint(),restaurant,16,start_computing_time);
                //this is the case when we get the result from A star using 16 directions
                if(fps!=null){
                    restaurants_flightPaths_map.replace(key,fps);
                    chosen_num_directions.replace(curr_r.getName(),16);
                } else {
                    //compute again using 8 directions
                    List<FlightPath> fps_8 = AStar(Utils.getStartPoint(),restaurant,8,start_computing_time);
                    if(fps_8!=null){
                        restaurants_flightPaths_map.replace(key,fps_8);
                        chosen_num_directions.replace(curr_r.getName(),8);
                    }
                }
            }
            System.out.println("Second A* algorithm has processed Restaurant "+curr_r.getName());
            long end = System.currentTimeMillis();
            restaurants_AvgComputingTime_map.put(curr_r.getName(), (int) (restaurants_AvgComputingTime_map.get(key)+(end-cur_time)));
        }

        //set up the average time for computing
        OrderAnalyze();
        ComputeAverageComputationTime();
    }

    /**
     * This method is used to analyze the information in restaurants_flightPaths_map and chosen_num_directions.
     * This method will print out the number of steps it takes to the restaurant and back the starting point in the restaurants_flightPaths_map
     * This method will also print out how many directions we chose to compute the A* algorithm in chosen_num_directions.
     */
    private static void OrderAnalyze(){
        System.out.println("\nAlgorithm result analysis");
        for (String key:restaurants_flightPaths_map.keySet()){
            System.out.println("The restaurant "+key+" needs "+restaurants_flightPaths_map.get(key).size()+ " steps using "+chosen_num_directions.get(key)+" directions to compute A* ");
        }
    }

    /**
     * This method is used to set up the average time for computing a single flight path.
     * We only add the total computation time after we received the result, so we need to compute average time using avg_time = Total_time/number of flight paths
     */
    private static void ComputeAverageComputationTime(){
        for (String key:restaurants_flightPaths_map.keySet()){
            //if the computation time is 0, to avoid denominator being 0, we add 1 ms to it
            if(restaurants_AvgComputingTime_map.get(key)/restaurants_flightPaths_map.get(key).size()==0){
                restaurants_AvgComputingTime_map.replace(key,restaurants_AvgComputingTime_map.get(key)/restaurants_flightPaths_map.get(key).size()+1);
            }else {
                restaurants_AvgComputingTime_map.replace(key,restaurants_AvgComputingTime_map.get(key)/restaurants_flightPaths_map.get(key).size());
            }
        }
    }

    /**
     * This function helps to sort the restaurants_flightPaths according to its size <br>
     * For example: The following is a hashmap(String, List(FlightPath)) <br>
     *              hashmap = ["restaurant1": flightPaths1,"restaurant2":flightPaths2] <br>
     *              if the size of flightPaths1 is bigger than that of flightPaths2 <br>
     *              then the sorted hashmap will be <br>
     *              ["restaurant2":flightPaths2,"restaurant1": flightPaths1] <br>
     */
    private static void SortRestaurantsFlightPathsByStepSize(){
        List<Map.Entry<String, List<FlightPath>>> list = new LinkedList<>(restaurants_flightPaths_map.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, List<FlightPath>> >() {
            public int compare(Map.Entry<String, List<FlightPath>> o1,
                               Map.Entry<String, List<FlightPath>> o2)
            {
                if(o1.getValue() == null){
                    return 1;
                }
                if(o2.getValue() == null){
                    return -1;
                }
                int diff = o1.getValue().size()-o2.getValue().size();
                if(diff>0){
                    return 1;
                }else if (diff==0){
                    return 0;
                }else {
                    return -1;
                }
            }
        });
        HashMap<String, List<FlightPath>> temp = new LinkedHashMap<>();
        for (Map.Entry<String, List<FlightPath>> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        restaurants_flightPaths_map = temp;
    }

    /**
     * This method processes the orders with the OrderOutcome "ValidButNotDelivered" and generate the flight paths.
     * To be more specific: <br>
     * We select the orders with 2 properties satisfied, one is OrderOutcome = "ValidButNotDelivered", second is delivery_restaurant= restaurant, and the restaurant here is the one with the least step sizes <br>
     * We try to deliver these orders if the remaining steps is enough
     */
    private static void ProcessingOrdersFlightPaths(){
        int start_processing_time = 0;
        for (String key : restaurants_flightPaths_map.keySet()){
            String curr_best_restaurant = key;
            Integer curr_restaurant_avgTimes = restaurants_AvgComputingTime_map.get(curr_best_restaurant);
            List<FlightPath> curr_best_restaurant_fps = restaurants_flightPaths_map.get(key);
            List<Order> curr_satisfied_orders = Utils_Order.findOrdersWithProperties(curr_best_restaurant, OrderOutcomes.ValidButNotDelivered);
            for (int i=0; i<curr_satisfied_orders.size(); i++){
                Order curr_order = curr_satisfied_orders.get(i);
                if (remainingSteps < curr_best_restaurant_fps.size()){
                    curr_best_steps = curr_best_restaurant_fps.size();
                    return;
                }
                for (int j=0; j<curr_best_restaurant_fps.size(); j++){
                    FlightPath curr_restaurant_fp = curr_best_restaurant_fps.get(j);
                    FlightPath curr_order_fp = new FlightPath(curr_restaurant_fp.getFromLongitude(),curr_restaurant_fp.getFromLatitude(),curr_restaurant_fp.getAngle(),curr_restaurant_fp.getToLongitude(),curr_restaurant_fp.getToLatitude());
                    curr_order_fp.setOrderNo(curr_order.getOrderNo());
                    curr_order_fp.setTicksSinceStartOfCalculation(start_processing_time+curr_restaurant_avgTimes);
                    orders_flightPaths.add(curr_order_fp);
                    start_processing_time = start_processing_time + curr_restaurant_avgTimes;
                }
                start_processing_time = orders_flightPaths.get(orders_flightPaths.size()-1).getTicksSinceStartOfCalculation();
                remainingSteps = remainingSteps -curr_best_restaurant_fps.size();
                curr_order.setOrderOutcome(OrderOutcomes.Delivered);
            }
        }
    }

    /**
     * This method is the logics of the whole flight Paths calculation. <br>
     * Steps: <br>
     * First step is to calculate the flight paths to each participating restaurants. <br>
     * Second step is to sort the flight paths of each participating restaurants by step sizes. <br>
     * Third step is that we prioritize smaller delivery_restaurant step size orders
     */
    public static void computeFlightPaths(){
        CalculatingFlightPathsToRestaurant();
        SortRestaurantsFlightPathsByStepSize();
        ProcessingOrdersFlightPaths();
    }

    /**
     * This is the path finding algorithm we apply.
     * This algorithm is called A star algorithm or A* (pronounced "A-star"), and it is a graph traversal and path search algorithm,
     * which is used in many fields of computer science due to its completeness, optimality, and optimal efficiency.
     * @param start this is the start point of the algorithm
     * @param end this is the end point of the algorithm
     * @param num_directions how many directions do we apply for this A* algorithm
     * @param global_start_time this is the time when we start the first computation
     * @return a list of FlightPath which indicates the flight path it takes from start point to end point
     */
    private static List<FlightPath> AStar(LngLat start, LngLat end, int num_directions, long global_start_time){
        long start_time = System.currentTimeMillis();

        if(start==null|end==null){
            return null;
        }

        //compute step size, step size indicates how many steps we ignore in the java for loop when computing next possible solutions
        //step_size=1 indicates 16 original directions, step_size=2 indicates 8 directions(E,NE,N,NW,W,SW,S,SE), step_size=4 indicates 4 directions(E,N,W,S)
        int step_size;
        if(num_directions==4){
            step_size=4;
        } else if (num_directions==8) {
            step_size=2;
        } else{
            step_size=1;
        }

        //find if the drone can leave central areas
        boolean canLeaveCentralAreas;
        if(start.inCentralAreas()==end.inCentralAreas()){
            canLeaveCentralAreas = false;
        }else {
            canLeaveCentralAreas = true;
        }

        //initialize the start point
        Node start_point = new Node(start,0, Heuristic(start,end), Heuristic(start,end),null,null,canLeaveCentralAreas);

        //initialize the Open set and Close set
        List<Node> open_list = new ArrayList<>();
        var close_set = new Hashtable<LngLat, Double>();
        open_list.add(start_point);
        close_set.put(start, 0.0);

        while (open_list.size()>0){
            long curr_time = System.currentTimeMillis();
            //if the time for this A star algorithm is more than the maximum time return
            if(curr_time - start_time > maximum_time_computingAStar){
                return null;
            }
            //if the time left for all computation is not enough, return
            if(curr_time - global_start_time > maximum_time_computingRestaurantsFlightPaths){
                return null;
            }

            //Sort and take the best node
            Collections.sort(open_list);
            Node curr_node = open_list.get(0);
            boolean canLeaveOrEnter = curr_node.getCanLeaveOrEnter_centralAreas();
            LngLat curr_position = curr_node.getCurr_node_location();
            //end condition
            if(curr_node.getCurr_node_location().closeTo(end)){
                break;
            }
            //Remove this best node
            open_list.remove(0);

            //find valid next step
            DroneDirections[] directions = DroneDirections.values();
            for(int i=0; i<directions.length; i=i+step_size){
                DroneDirections d = directions[i];
                LngLat nextPosition = curr_position.nextPosition(d);
                //compute if violates no-fly zones and central areas
                boolean violates_noFlyZones = curr_position.checkIfViolatesNoFlyZonesRule(nextPosition);
                Map<String, Boolean> violates_centralAreas_maps =curr_position.checkIfViolatesCentralAreaRule(nextPosition, canLeaveOrEnter);
                boolean violates_centralAreas = violates_centralAreas_maps.get("ViolatesRule");
                Boolean canLeaveOrEnterAgain = violates_centralAreas_maps.get("canLeaveOrEnter");

                if(violates_centralAreas|violates_noFlyZones){
                    continue;
                }

                //compute next steps g_cost and f_cost
                double g_cost_next = curr_node.getG_cost()+1;
                double h_cost_next = Heuristic(nextPosition,end);
                double f_cost_next = g_cost_next+h_cost_next;

                //compute if the next position is inside the close-set
                if(close_set.containsKey(nextPosition)){
                    if(g_cost_next<close_set.get(nextPosition)){
                        close_set.replace(nextPosition,g_cost_next);
                    }else {
                        continue;
                    }
                }else {
                    close_set.put(nextPosition,g_cost_next);
                }

                Node nextNode = new Node(nextPosition,g_cost_next,h_cost_next,f_cost_next,d,curr_node,canLeaveOrEnterAgain);

                //check if the next Position is inside the open list
                boolean inOpenList = false;
                for (int j=0; j<open_list.size(); j++){
                    Node curr_open_node = open_list.get(j);
                    if(curr_open_node.getCurr_node_location().equals(nextPosition)){
                        if(curr_open_node.getF_cost()>f_cost_next){
                            open_list.remove(j);
                        }else {
                            inOpenList= true;
                            break;
                        }
                    }
                }
                if(!inOpenList){
                    open_list.add(nextNode);
                }
            }
        }
        //This is the case when we don't find any valid paths
        if(open_list.size()==0){
            return null;
        }
        //otherwise this is when we find the path
        Node best_node = open_list.get(0);
        return Reconstruct_flightPath(best_node);
    }

    /**
     * This method is used to calculate the steps from restaurant to Appleton Tower.
     * We can just simply reverse the flight path of AppleTon Tower to restaurant.
     * @param fps the given flight paths which describes the steps from Appleton Tower to Restaurant
     * @return the flight paths from restaurant to Appleton Tower
     */
    private static ArrayList<FlightPath> ReverseFlightPath(List<FlightPath> fps){
        ArrayList<FlightPath> reversed_fps = new ArrayList<>();

        //Traverse the whole flight paths and swap the fromPoint with ToPoint
        for(int i=fps.size()-1; i>=0; i--){
            FlightPath curr_fp = fps.get(i);
            FlightPath reversed_fp = new FlightPath(curr_fp.getToLongitude(),curr_fp.getToLatitude(), (curr_fp.getAngle()+180)%360, curr_fp.getFromLongitude(),curr_fp.getFromLatitude());
            reversed_fps.add(reversed_fp);
        }
        return reversed_fps;
    }

    /**
     * This method is used to generate a list of flight paths using a Node class
     * @param curr_node the Node that needs to generate the flight paths
     * @return list of flight paths which indicates the flight path of the drone from the launch point to the destination restaurant and back to the launch point.
     */
    private static List<FlightPath> Reconstruct_flightPath(Node curr_node){
        List<FlightPath> fps = new ArrayList<>();

        //Using the Node to construct a list of Flight Path class
        while (curr_node.getPrevious_node() != null) {
            Node previous_node = curr_node.getPrevious_node();
            LngLat previous_node_location = previous_node.getCurr_node_location();
            LngLat curr_node_location = curr_node.getCurr_node_location();
            double angles = curr_node.getAngles_to_curr_node().getAngles();
            FlightPath fp = new FlightPath(previous_node_location.getLng(), previous_node_location.getLat(), angles, curr_node_location.getLng(), curr_node_location.getLat());
            fps.add(fp);
            curr_node = previous_node;
        }

        //Reverse the list of flight paths because, the first location stored in the Node is the end position, so we need to reverse them
        Collections.reverse(fps);

        //Computing the hover operation for end position(which is restaurant)
        FlightPath last_element = fps.get(fps.size() - 1);
        FlightPath hover_end_point = new FlightPath(last_element.getToLongitude(), last_element.getToLatitude(), Utils.HOVER_ANGLE, last_element.getToLongitude(), last_element.getToLatitude());

        //We need to reverse the flight paths again to compute the flight paths from the end position(restaurant) to the start position(Appleton Tower)
        List<FlightPath> reversed_fps = ReverseFlightPath(fps);

        //Computing the hover point for the starting point(which is Appleton Tower)
        FlightPath reversed_last_element = reversed_fps.get(reversed_fps.size() - 1);
        FlightPath hover_start_point = new FlightPath(reversed_last_element.getToLongitude(), reversed_last_element.getToLatitude(), Utils.HOVER_ANGLE, reversed_last_element.getToLongitude(), reversed_last_element.getToLatitude());

        //Add these operations to the fps in the following order, and this is the flight paths of the drone from the launch point to the destination restaurant and back to the launch point.
        fps.add(hover_end_point);
        fps.addAll(reversed_fps);
        fps.add(hover_start_point);

        return fps;
    }

    /**
     * This is the heuristic function for the A* algorithm.
     * We just take the Euclidean distance of the start point and end point divides the step_per_move as our heuristic value
     * @param start the point that we need to calculate the heuristic value
     * @param end the end position
     * @return the heuristic value for start point
     */
    public static double Heuristic(LngLat start, LngLat end){
        return start.distanceTo(end)/Utils.MAXIMUM_STEP_PER_MOVE;
    }

}

