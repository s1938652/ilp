package uk.ac.ed.inf.utils;

import uk.ac.ed.inf.eunms.OrderOutcomes;
import uk.ac.ed.inf.exceptions.InvalidPizzaCombinationException;
import uk.ac.ed.inf.exceptions.OrderItemNotDefinedException;
import uk.ac.ed.inf.classes.Order;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is a static class that stores some useful methods that can’t be included in the Order class.
 */
public class Utils_Order {
    public static int wrong_num;

    /**
     * checkOrders will validate all the orders fetched from the server and assign an order outcome to the order
     */
    public static void checkOrders(){
        //check if the orders is null
        if (Client.getOrders_instance() == null){
            throw new NullPointerException("Please get some orders first");
        }

        //Traverse all the order in the orders
        for (int i = 0; i<Client.getOrders_instance().size(); i++){
            Order order = Client.getOrders_instance().get(i);

            //checks if the current order has null attributes
            if ((order.getCustomer()==null)|(order.getOrderItems()==null)|(!order.getOrderDate().equals(Utils.getDelivery_date()))){
                order.setOrderOutcome(OrderOutcomes.Invalid);
                continue;
            }

            //checks if the cvv of the card is valid
            if (!cvvIsValid(order.getCvv())){
                order.setOrderOutcome(OrderOutcomes.InvalidCvv);
                continue;
            }

            if(Integer.parseInt(order.getCreditCardExpiry().split("/")[1])==Integer.parseInt(Utils.getDelivery_date().split("-")[0].substring(2))){
                wrong_num = wrong_num+1;
            }

            //checks if the given card is expired or not
            if (!cardIsNotExpired(order.getCreditCardExpiry())){
                order.setOrderOutcome(OrderOutcomes.InvalidExpiryDate);
                continue;
            }

            //checks whether a given card number is valid or not
            if (!cardNumberIsValid(order.getCreditCardNumber())){
                order.setOrderOutcome(OrderOutcomes.InvalidCardNumber);
                continue;
            }

            //checks if the number of pizzas in the order is valid or not
            if ((order.getOrderItems().length > Utils.MAXIMUM_PIZZA_PER_DELIVERY)|
                    (order.getOrderItems().length < Utils.MINIMUM_PIZZA_PER_DELIVERY))
            {
                order.setOrderOutcome(OrderOutcomes.InvalidPizzaCount);
                continue;
            }

            //set the delivery restaurant if we can't set them and raise exceptions, we just catch these and set the order outcomes
            try {
                order.setDeliver_restaurant();
            } catch (OrderItemNotDefinedException e) {
                order.setOrderOutcome(OrderOutcomes.InvalidPizzaNotDefined);
                continue;
            } catch (InvalidPizzaCombinationException e) {
                order.setOrderOutcome(OrderOutcomes.InvalidPizzaCombinationMultipleSuppliers);
                continue;
            }

            //compute the real cost
            int cost = order.computeRealCost();

            //This is to check if the total cost is correct
            if (cost!=order.getPriceTotalInPence()){
                order.setOrderOutcome(OrderOutcomes.InvalidTotal);
                continue;
            }

            //If the order is valid
            order.setOrderOutcome(OrderOutcomes.ValidButNotDelivered);
        }
    }

    /**
     * This function gives the list of orders which has the same properties(delivery restaurant, Order outcome)
     * with the input parameters
     * @param r the given restaurant's name
     * @param oc the given Order Outcome
     * @return the list of orders which has the same properties with the input
     */
    public static List<Order> findOrdersWithProperties(String r, OrderOutcomes oc){
        List<Order> satisfied_orders = new ArrayList<>();
        for (int i = 0; i<Client.getOrders_instance().size(); i++){
            Order curr_order = Client.getOrders_instance().get(i);
            if (curr_order.getOrderOutcome() == oc && curr_order.getDeliver_restaurant() == r){
                satisfied_orders.add(curr_order);
            }
        }
        return satisfied_orders;
    }

    /**
     * This function computes the number of satisfied order outcome in the orders
     * @param oc the given order outcome
     * @return the number of satisfied orders
     */
    public static int checkNumberOfOrdersWithSatisfiedOC(OrderOutcomes oc){
        int sum = 0;
        for (int i = 0; i<Client.getOrders_instance().size(); i++){
            Order curr_order = Client.getOrders_instance().get(i);
            if (curr_order.getOrderOutcome() == oc){
                sum = sum+1;
            }
        }
        return sum;
    }

    //some basic operations to check the order's validity
    /**
     * This function computes if cvv is correct or not in the order class
     * @param cvv a string which represents the CVV
     * @return True if CVV is valid, vice versa
     */
    public static Boolean cvvIsValid(String cvv){
        if (cvv == null){
            throw new NullPointerException();
        }
        if (cvv.length()!=3){
            return Boolean.FALSE;
        }
        char[] chars = cvv.toCharArray();
        for(char c : chars){
            if(!Character.isDigit(c)){
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    /**
     * This function checks if a credit card is already expired
     * @param creditCardExpiry the expiry date of the card
     * @return True if the card is not expired, False if the card is expired
     */
    public static Boolean cardIsNotExpired(String creditCardExpiry) {
        if ((Utils.getDelivery_date() == null)|(creditCardExpiry==null)){
            throw new NullPointerException();
        }
        //This splits the input into two parts, the year and the month
        String[] credit_card_MY = creditCardExpiry.split("/");
        //check if it successfully splits the input into two parts
        if (credit_card_MY.length != 2){
            return Boolean.FALSE;
        }
        //split the order dates into 3 parts, the year, the month, the day
        String[] order_MY = Utils.getDelivery_date().split("-");

        //This checks if the expiry year of credit card is bigger or equal to the order date
        if (Integer.parseInt(credit_card_MY[1]) < Integer.parseInt(order_MY[0].substring(2))){
            return Boolean.FALSE;
        }
        else if(Integer.parseInt(credit_card_MY[1]) == Integer.parseInt(order_MY[0].substring(2))){
            //This checks whether the expiry month is bigger or equal to the order date
            if (Integer.parseInt(credit_card_MY[0])<Integer.parseInt(order_MY[1])){
                return Boolean.FALSE;
            }
        }
        //if everything is ok, then return the expiry date valid
        return Boolean.TRUE;
    }

    /**
     * This function checks if a given card number is valid or not
     * @return true if the card is valid, vice versa
     */
    public static boolean cardNumberIsValid(String cardNo){
        if (cardNo == null){
            throw new NullPointerException();
        }
        int number_of_digits = cardNo.length();
        if (number_of_digits != 16){
            return false;
        }
        int Sum = 0;
        boolean isSecond = false;
        //traverse the digits
        for (int i = number_of_digits - 1; i >= 0; i--) {
            int a = cardNo.charAt(i) - '0';
            if (isSecond)
                a = a * 2;
            Sum += a / 10;
            Sum += a % 10;
            isSecond = !isSecond;
        }
        return (Sum % 10 == 0);
    }

}
