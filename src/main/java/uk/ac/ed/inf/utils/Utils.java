package uk.ac.ed.inf.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import uk.ac.ed.inf.eunms.OrderOutcomes;
import uk.ac.ed.inf.classes.*;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class is a static class that is used to store static methods and Constants that can be used by the entire program. <br>
 * This class has several public fields: <br>
 * TOLERANCE_DEGREES, MAXIMUM_STEP_PER_MOVE, MAXIMUM_PIZZA_PER_DELIVERY, MAXIMUM_PIZZA_PER_DELIVERY, DELIVERY_FEE, APPLETON_TOWER, HOVER_ANGLE, MAXIMUM_ERROR <br>
 * This class has several private property:<br>
 * base_url, random_str, delivery_date, remainingSteps <br>
 * This class has two public methods: <br>
 * computeUniqueRestaurantsInOrders, writeJsonFile
 */
public class Utils {
    /**
     * Distance tolerance in degrees
     */
    public final static double TOLERANCE_DEGREES = 0.00015;

    /**
     * Maximum number of moves a drone can make
     */
    public final static double MAXIMUM_STEP_PER_MOVE = 0.00015;

    /**
     * Maximum number of pizzas a drone can take for one order
     */
    public final static int MAXIMUM_PIZZA_PER_DELIVERY = 4;

    /**
     * Minimum number of pizzas a drone must take for one order
     */
    public final static int MINIMUM_PIZZA_PER_DELIVERY = 1;

    /**
     * The delivery cost of an order
     */
    public final static int DELIVERY_FEE = 100;

    /**
     * The location of the Appleton Tower
     */
    public final static LngLat APPLETON_TOWER = new LngLat(-3.1869,55.9445);


    /**
     * The maximum steps that the drone can move when battery is full
     */
    public static final int BATTERY = 2000;
    /**
     * Angle value when hovering
     */
    public final static Double HOVER_ANGLE = null;

    /**
     * We will assume 2 points are same if their euclidean distance is less or equal to this MAXIMUM_ERROR
     */
    public final static double MAXIMUM_ERROR = 0.000015;

    /**
     * This is the location of generated Json file
     */
    public final static String RESULT_FILES_LOC = "./resultfiles/";

    /**
     * private property which indicates the date of delivery
     */
    private static String delivery_date;

    /**
     * private property which indicates the base url
     */
    private static URL baseURL;

    /**
     * The starting position of the drone
     */
    private static LngLat startPoint;

    public static LngLat getStartPoint() {return startPoint;}

    public static void setStartPoint(LngLat startPoint) {Utils.startPoint = startPoint;}

    /**
     * Getter method for accessing the private property baseURL
     * @return the value of baseURL
     */
    public static URL getBaseURL() {return baseURL;}

    /**
     * Getter method for accessing the private property delivery_date
     * @return the value of delivery_date
     */
    public static String getDelivery_date() {return delivery_date;}

    /**
     * Setter method for setting the baseURL
     * @param baseURL the baseURL from the command line
     */
    public static void setBaseURL(URL baseURL) {Utils.baseURL = baseURL;}

    /**
     * Setter method for setting the delivery_date
     * @param delivery_date the delivery date from the command line
     */
    public static void setDelivery_date(String delivery_date) {Utils.delivery_date = delivery_date;}

    /**
     * This method computes the unique restaurants that take part in the orders. <br>
     * For example: The following is a list of Order class <br>
     *              List1 = [Order1, Order2, Order3, Order4] <br>
     *              The following is the list of corresponding private property delivery_restaurant <br>
     *              List2 = [Restaurant_name1, Restaurant_name1, Restaurant_name2, Restaurant_name2] <br>
     *              After this function we will return list = [Restaurant1, Restaurant2]
     *
     * @return list of Restaurants which represents the unique restaurants that take part in the orders
     */
    public static List<Restaurant> computeUniqueRestaurantsInOrders(){
        List<Restaurant> restaurant_needed_calculated = new ArrayList<>();

        //compute all the restaurants that occurred in the Valid orders
        for (int i = 0; i<Client.getOrders_instance().size(); i++){
            Order curr_order = Client.getOrders_instance().get(i);
            if(curr_order.getDeliver_restaurant() != null && curr_order.getOrderOutcome()== OrderOutcomes.ValidButNotDelivered){
                restaurant_needed_calculated.add(Utils_Restaurant.getRestaurantByName(curr_order.getDeliver_restaurant()));
            }
        }

        //make the restaurants_needed_calculated unique
        List<Restaurant> restaurants = restaurant_needed_calculated.stream().distinct().collect(Collectors.toList());
        return restaurants;
    }

    /**
     * This method helps to generate the geoJson format of the drone's Flight Paths
     * @return JSONObject which describes the drone's Flight Paths
     */
    private static JSONObject FlightPath_to_geoJson(){
        //flight paths
        JSONObject flightPaths_json = new JSONObject();
        if (Utils_FlightPath.getOrders_flightPaths().size() == 0){
            return null;
        }
        flightPaths_json.put("type","Feature");
        flightPaths_json.put("properties",new JSONObject());
        JSONObject flightPaths_geometry = new JSONObject();
        flightPaths_geometry.put("type","LineString");
        List<List<Double>> coordinates_flightPaths = new ArrayList<>();

        //Adding the first flight path's fromLongitude and fromLatitude
        List<Double> coordinate_flightPath0 = new ArrayList<>();
        coordinate_flightPath0.add(Utils_FlightPath.getOrders_flightPaths().get(0).getFromLongitude());
        coordinate_flightPath0.add(Utils_FlightPath.getOrders_flightPaths().get(0).getFromLatitude());
        coordinates_flightPaths.add(coordinate_flightPath0);

        //Traverse all the flight path and add the toLongitude and toLatitude
        for (int i=0; i<Utils_FlightPath.getOrders_flightPaths().size(); i++){
            FlightPath curr_flightPath = Utils_FlightPath.getOrders_flightPaths().get(i);
            List<Double> coordinate_flightPath = new ArrayList<>();
            coordinate_flightPath.add(curr_flightPath.getToLongitude());
            coordinate_flightPath.add(curr_flightPath.getToLatitude());
            coordinates_flightPaths.add(coordinate_flightPath);
        }
        flightPaths_geometry.put("coordinates",coordinates_flightPaths);
        flightPaths_json.put("geometry",flightPaths_geometry);
        return flightPaths_json;
    }

    /**
     * This function generates the flightpath-YYYY-MM-DD.json file which describes the flight path
     */
    private static void Write_flightpath_JSON(String loc){
        JsonArray FlightPaths_json = new JsonArray();

        for(int i = 0; i<Utils_FlightPath.getOrders_flightPaths().size(); i++){
            FlightPath curr_fp = Utils_FlightPath.getOrders_flightPaths().get(i);
            JsonObject fp_json = new JsonObject();
            fp_json.addProperty("orderNo", curr_fp.getOrderNo());
            fp_json.addProperty("fromLongitude", curr_fp.getFromLongitude());
            fp_json.addProperty("fromLatitude", curr_fp.getFromLatitude());
            fp_json.addProperty("angle",curr_fp.getAngle());
            fp_json.addProperty("toLongitude",curr_fp.getToLongitude());
            fp_json.addProperty("toLatitude", curr_fp.getToLatitude());
            fp_json.addProperty("ticksSinceStartOfCalculation",curr_fp.getTicksSinceStartOfCalculation());
            FlightPaths_json.add(fp_json);
        }
        try(FileWriter file = new FileWriter(loc +"flightpath-"+ delivery_date +".json")) {
            file.write(FlightPaths_json.toString());
        }catch (IOException e){e.printStackTrace();}
    }


    /**
     * This function generates the drone-YYYY-MM-DD.geojson file which describes the flight Path
     */
    private static void Write_Drone_FlightPath_GEOJSON(String loc){
        JSONObject all = new JSONObject();
        all.put("type","FeatureCollection");

        JSONArray features = new JSONArray();
        JSONObject flightPathsJson = FlightPath_to_geoJson();
        if (flightPathsJson != null){
            features.add(flightPathsJson);
        }
        //put the overall features inside the json object
        all.put("features",features);
        //write the GeoJson file
        try {
            FileWriter file = new FileWriter(loc +"drone-"+ delivery_date +".geojson");
            file.write(all.toString());
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This function generates the deliveries-YYYY-MM-DD.json file which describes the order
     */
    private static void Write_deliveries_JSON(String loc) {
        JsonArray deliveries_json = new JsonArray();
        if(Client.getOrders_instance()!=null){
            for (int i = 0; i < Client.getOrders_instance().size(); i++) {
                Order curr_order = Client.getOrders_instance().get(i);
                JsonObject delivery_json = new JsonObject();
                delivery_json.addProperty("orderNo", curr_order.getOrderNo());
                delivery_json.addProperty("outcome", curr_order.getOrderOutcome().name());
                delivery_json.addProperty("costInPence", curr_order.getPriceTotalInPence());
                deliveries_json.add(delivery_json);
            }
        }
        try (FileWriter file = new FileWriter(loc +"deliveries-" + delivery_date + ".json")) {
            file.write(deliveries_json.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method helps to create the 3 files.
     * The first one is deliveries-yyyy-MM-dd.json
     * The second one is drone-yyyy-MM-dd.geojson
     * The third one is flightpath-yyyy-MM-dd.json
     */
    public static void writeJsonFile(String loc){
        Write_flightpath_JSON(loc);
        Write_Drone_FlightPath_GEOJSON(loc);
        Write_deliveries_JSON(loc);
    }


}