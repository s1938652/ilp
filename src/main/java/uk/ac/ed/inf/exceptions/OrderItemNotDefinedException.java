package uk.ac.ed.inf.exceptions;

/**
 * OrderItemNotDefinedException is a class that extends the Exception class
 * and is thrown when some of ordered pizzas are undefined(meaning not from any of the restaurants)
 * @author cuijiacheng on 27/11/2022
 */
public class OrderItemNotDefinedException extends Exception{
    /**
     * This is the constructor of the class which uses the parent's constructor to initialize
     * @param error the error message
     */
    public OrderItemNotDefinedException(String error){
        super(error);
    }
}
