package uk.ac.ed.inf.exceptions;

/**
 *InvalidPizzaCombinationException is a class that extends the Exception class
 * and is thrown when the ordered pizzas are not from the same restaurant.
 * @author cuijiacheng on 27/11/2022
 */
public class InvalidPizzaCombinationException extends Exception{
    /**
     * This is the constructor of the class which uses the parent's constructor to initialize
     * @param error the error message
     */
    public InvalidPizzaCombinationException(String error){super(error);}
}
