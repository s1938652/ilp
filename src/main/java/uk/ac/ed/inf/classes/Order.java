package uk.ac.ed.inf.classes;

import com.fasterxml.jackson.annotation.JsonProperty;
import uk.ac.ed.inf.eunms.OrderOutcomes;
import uk.ac.ed.inf.exceptions.InvalidPizzaCombinationException;
import uk.ac.ed.inf.exceptions.OrderItemNotDefinedException;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;
import uk.ac.ed.inf.utils.Utils_Restaurant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This class is used to store information about orders fetched from the server. <br>
 * There are 11 private properties in the Order class: orderNo(String), orderDate(String), customer(String), creditCardNumber(String),
 * creditCardExpiry(String), cvv(String), priceTotalInPence(int), orderItems(String[]), orderOutcome(OrderOutcome), deliver_restaurant(String).
 */
public class Order {

    /**
     * This private property orderNo indicates the orderNo of the current order
     */
    @JsonProperty("orderNo")
    private String orderNo;

    /**
     * This private property orderDate indicates the orderDate of the current order
     */
    @JsonProperty("orderDate")
    private String orderDate;

    /**
     * This private property customer indicates the customer of the current order
     */
    @JsonProperty("customer")
    private String customer;

    /**
     * This private property creditCardNumber indicates the card number that used to purchase this order
     */
    @JsonProperty("creditCardNumber")
    private String creditCardNumber;

    /**
     * This private property creditCardExpiry indicates the expiry date of the card that used to purchase this order
     */
    @JsonProperty("creditCardExpiry")
    private String creditCardExpiry;

    /**
     * This private property cvv indicates the cvv of the card that used to purchase this order
     */
    @JsonProperty("cvv")
    private String cvv;

    /**
     * This private property priceTotalInPence indicates the total price of this order
     */
    @JsonProperty("priceTotalInPence")
    private int priceTotalInPence;

    /**
     * This private property indicates the ordered items in the current order
     */
    @JsonProperty("orderItems")
    private String[] orderItems;

    /**
     * This private property indicates the outcome of the current order
     */
    private OrderOutcomes orderOutcome;

    /**
     * This private property indicates which restaurant is involved in this order
     */
    private String deliver_restaurant;

    /**
     * This is the Getter method to access the private property orderNo
     * @return the value of private property orderNo
     */
    public String getOrderNo() {return orderNo;}

    /**
     * This is the Getter method to access the private property orderDate
     * @return the value of private property orderDate
     */
    public String getOrderDate() {return orderDate;}

    /**
     * This is the Getter method to access the private property customer
     * @return the value of private property customer
     */
    public String getCustomer() {return customer;}

    /**
     * This is the Getter method to access the private property creditCardNumber
     * @return the value of private property creditCardNumber
     */
    public String getCreditCardNumber() {return creditCardNumber;}

    /**
     * This is the Getter method to access the private property creditCardExpiry
     * @return the value of private property creditCardExpiry
     */
    public String getCreditCardExpiry() {return creditCardExpiry;}

    /**
     * This is the Getter method to access the private property cvv
     * @return the value of private property cvv
     */
    public String getCvv() {return cvv;}

    /**
     * This is the Getter method to access the private property priceTotalInPence
     * @return the value of private property priceTotalInPence
     */
    public int getPriceTotalInPence() {return priceTotalInPence;}

    /**
     * This is the Getter method to access the private property orderItems
     * @return the value of private property orderItems
     */
    public String[] getOrderItems() {return orderItems;}

    /**
     * This is the Getter method to access the private property orderOutcome
     * @return the value of private property orderOutcome
     */
    public OrderOutcomes getOrderOutcome() {return orderOutcome;}

    /**
     * This is the Getter method to access the private property deliver_restaurant
     * @return the value of private property deliver_restaurant
     */
    public String getDeliver_restaurant() {return deliver_restaurant;}

    /**
     * This is the Setter method for the private property orderOutcomes
     * @param orderOutcome the order outcome for the current order
     */
    public void setOrderOutcome(OrderOutcomes orderOutcome) {this.orderOutcome = orderOutcome;}

    /**
     * This is the Setter method fot the private property deliver_restaurant.
     * This method will try to find the restaurant involved in this order, but we may not find this and raise the following exceptions
     * @throws OrderItemNotDefinedException when the ordered pizzas are not from the same restaurant.
     * @throws InvalidPizzaCombinationException when some of ordered pizzas are undefined(meaning not from any of the restaurants)
     */
    public void setDeliver_restaurant() throws OrderItemNotDefinedException, InvalidPizzaCombinationException {
        int[] order_Item_occurrence = new int[orderItems.length];
        //Denotes which restaurant we are currently accessing
        int index_for_restaurants = 0;
        while (index_for_restaurants < Client.getRestaurants_instance().size()) {
            Restaurant curr_restaurant = Client.getRestaurants_instance().get(index_for_restaurants);
            //To check if all of ordered pizzas name is in the current restaurant's menu
            if (Utils_Restaurant.allOrderItemsInMenus(orderItems,curr_restaurant)) {
                //assign the current's name to the deliver_restaurant field
                deliver_restaurant = curr_restaurant.getName();
                return;
            }
            List<Integer> curr_occurred_order_item = Utils_Restaurant.whichOrderItemsInMenus(orderItems,curr_restaurant);
            for (int j=0; j<curr_occurred_order_item.size(); j++){
                order_Item_occurrence[curr_occurred_order_item.get(j)] = 1;
            }
            //If the menu of the current restaurant does not have any of the order item, go to next restaurant
            index_for_restaurants++;
        }
        //this case is all the order item occurs in the menu, but not from same restaurant
        if(Arrays.stream(order_Item_occurrence).sum() == orderItems.length){
            throw new InvalidPizzaCombinationException("the order items is not from same restaurant");
        }
        //this case is when some order item is undefined
        else{
            throw new OrderItemNotDefinedException("the order items is undefined");
        }
    }

    /**
     * This method is used to compute the real cost of an order. <br>
     * This method will find the menus inside the private property deliver_restaurant to find the real cost of an order
     * @return the real cost of this order
     */
    public int computeRealCost(){
        if(deliver_restaurant==null){
            throw new NullPointerException();
        }
        //The total cost of the order is defined here
        int total_cost = 0;
        Restaurant restaurant = Utils_Restaurant.getRestaurantByName(deliver_restaurant);
        if(restaurant==null){
            throw new NullPointerException();
        }
        Map<String, Integer> namePrice_map = Utils_Restaurant.computeNamePrice(restaurant);
        //traverse all the order items and compute the total cost
        for (int i = 0; i < orderItems.length; i++) {
            total_cost = total_cost + namePrice_map.get(orderItems[i]);
        }
        return total_cost + Utils.DELIVERY_FEE;
    }

}
