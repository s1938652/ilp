package uk.ac.ed.inf.classes;

import uk.ac.ed.inf.eunms.DroneDirections;

/**
 * This class is used to store information about the routes to the current Node. <br>
 * The Node class has 7 private properties:
 * g_cost, h_cost, f_cost, curr_node_location, previous_node, angle_to_curr_node, canLeaveOrEnter_centralAreas
 * @author cuijiacheng on 20/11/2022
 */
public class Node implements Comparable<Node>{
    /**
     * The private property g_cost denotes the step size we take to the current position
     */
    private double g_cost;

    /**
     * The private property h_cost denotes the heuristic cost to the end position
     */
    private double h_cost;

    /**
     * The private property f_cost denotes the whole cost which equals g_cost+h_cost
     */
    private double f_cost;

    /**
     * The private property curr_node_location represents the location of the current position
     */
    private LngLat curr_node_location;

    /**
     * The private property previous_node means the parent node of the current node
     */
    private Node previous_node;

    /**
     * The private property angles_to_curr_node means what direction the drone takes to reach the current node from the parent node
     */
    private DroneDirections angles_to_curr_node;

    /**
     * The private property canLeaveOrEnter_centralAreas means that if the drone is able to leave or enter the central areas at the current node.
     */
    private boolean canLeaveOrEnter_centralAreas;

    /**
     * This method is the constructor of a Node class
     * @param curr_node_location the current position of this Node
     * @param g_cost how much steps drone takes to reach the current node
     * @param h_cost the estimated steps the drone need to take to the end position
     * @param f_cost the total steps to take from the current node to the end (g_cost+h_cost)
     * @param angles_to_curr_node the angles it takes from the parent node to the current node
     * @param previous_node the parent node
     * @param canLeaveOrEnter_centralAreas can the drone leave or enter central areas at the current node
     */
    public Node(LngLat curr_node_location,double g_cost,double h_cost,double f_cost, DroneDirections angles_to_curr_node, Node previous_node, boolean canLeaveOrEnter_centralAreas){
        this.g_cost = g_cost;
        this.h_cost = h_cost;
        this.f_cost = f_cost;
        this.curr_node_location = curr_node_location;
        this.angles_to_curr_node = angles_to_curr_node;
        this.previous_node = previous_node;
        this.canLeaveOrEnter_centralAreas = canLeaveOrEnter_centralAreas;
    }

    /**
     * The Getter method for private property f_cost
     * @return the value of private property f_cost
     */
    public double getF_cost() {return f_cost;}

    /**
     * The Getter method for private property h_cost
     * @return the value of private property h_cost
     */
    public double getH_cost() {return h_cost;}

    /**
     * The Getter method for private property g_cost
     * @return the value of private property g_cost
     */
    public double getG_cost() {return g_cost;}

    /**
     * The Getter method for private property angles_to_curr_node
     * @return the value of private property angles_to_curr_node
     */
    public DroneDirections getAngles_to_curr_node() {return angles_to_curr_node;}

    /**
     * The Getter method for private property curr_node_location
     * @return the value of private property curr_node_location
     */
    public LngLat getCurr_node_location() {return curr_node_location;}

    /**
     * The Getter method for private property previous_node
     * @return the value of private property previous_node
     */
    public Node getPrevious_node() {return previous_node;}

    /**
     * The Getter method for private property canLeaveOrEnter_centralAreas
     * @return the value of private property canLeaveOrEnter_centralAreas
     */
    public boolean getCanLeaveOrEnter_centralAreas(){return this.canLeaveOrEnter_centralAreas;}

    /**
     * This method compares which Node has bigger f_cost
     * @param node the object to be compared.
     * @return 1 if the current Node has bigger f_cost<br>
     *         0 if the two Nodes have the same f_cost<br>
     *         -1 if the input Node has bigger f_cost
     */
    @Override
    public int compareTo(Node node){
        double diff = this.f_cost-node.f_cost;
        if( diff > 0){
            return 1;
        }else if(diff == 0){
            return 0;
        }else {
            return -1;
        }
    }


}
