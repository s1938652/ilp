package uk.ac.ed.inf.classes;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is used to store information about no-fly zone that was fetched from the Server. <br>
 * There are 2 private properties in this class: name(String), coordinates(double[][]) which represents the vertex of polygon
 */
public class NoFlyZone {
    /**
     * This private property name indicates the name of the no-fly zone
     */
    @JsonProperty("name")
    private String name;

    /**
     * This private property coordinates indicates the points that forms the no-fly zone
     */
    @JsonProperty("coordinates")
    private double[][] coordinates;

    /**
     * This is the Getter method for the private property name
     * @return the value of private property name
     */
    public String getName() {return name;}

    /**
     * This is the Getter method for the private property coordinates
     * @return the value of private property coordinates
     */
    public double[][] getCoordinates() {return coordinates;}

}
