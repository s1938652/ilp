package uk.ac.ed.inf.classes;

/**
 * This class is used to store information about a single movement of the drone. <br>
 * And a list of FlightPath classes can represent a sequence of movements of a drone. <br>
 * This class has 7 private properties: orderNo(String), fromLongitude(double), fromLatitude(double), angle(Double),
 * toLongitude(double), toLatitude(double),ticksSinceStartOfCalculation(int). <br>
 * Every property is required when writing the JSON files and GeoJson files about flight paths.
 */
public class FlightPath{
    /**
     * This private property indicates the orderNo of the order that the drone is currently delivering
     */
    private String orderNo;

    /**
     * This private property indicates the longitude of the drone's initial position
     */
    private double fromLongitude;

    /**
     * This private property indicates the latitude of the drone's initial position
     */
    private double fromLatitude;

    /**
     * This private property indicates the angle for drone to reach the end position from initial position
     */
    private Double angle;

    /**
     * This private property indicates the longitude of the drone's end position
     */
    private double toLongitude;

    /**
     * This private property indicates the latitude of the drone's end position
     */
    private double toLatitude;

    /**
     * This private property indicates how many milliseconds have passed since the first calculation starts
     */
    private int ticksSinceStartOfCalculation;

    /**
     * Getter method for the private property orderNo
     * @return the value of the private property orderNo
     */
    public String getOrderNo() {return orderNo;}

    /**
     * Getter method for the private property fromLongitude
     * @return the value of private property fromLongitude
     */
    public double getFromLongitude() {return fromLongitude;}

    /**
     * Getter method for the private property fromLatitude
     * @return the value of private property fromLatitude
     */
    public double getFromLatitude() {return fromLatitude;}

    /**
     * Getter method for the private property angle
     * @return the value of the private property angle
     */
    public Double getAngle() {return angle;}

    /**
     * Getter method for the private property toLongitude
     * @return the value of the private property toLongitude
     */
    public double getToLongitude() {return toLongitude;}

    /**
     * Getter method for the private property toLatitude
     * @return the value of private property toLatitude
     */
    public double getToLatitude() {return toLatitude;}

    /**
     * Getter method for the private property ticksSinceStartOfCalculation
     * @return the value of private property ticksSinceStartOfCalculation
     */
    public int getTicksSinceStartOfCalculation() {return ticksSinceStartOfCalculation;}

    /**
     * Setter method for the private property orderNo and it is used when processing the flight paths to deliver each orders.
     * @param orderNo the orderNo of current order we are delivering
     */
    public void setOrderNo(String orderNo) {this.orderNo = orderNo;}

    /**
     * Setter method for the private property ticksSinceStartOfCalculation and it is used when processing the flight paths to deliver each orders.
     * @param ticksSinceStartOfCalculation represents how much time has passed since we start the first calculation started
     */
    public void setTicksSinceStartOfCalculation(int ticksSinceStartOfCalculation) {this.ticksSinceStartOfCalculation = ticksSinceStartOfCalculation;}

    /**
     * This is the constructor of this class, and it only takes 5 parameters to initialize a FlightPath class.
     * @param fromLongitude indicates the longitude that the drone flies from
     * @param fromLatitude indicates the latitude that the drone flies from
     * @param angle indicates the angle it takes to fly from (fromLongitude, fromLatitude) to (toLongitude, toLatitude)
     * @param toLongitude indicates the longitude that the drone flies to
     * @param toLatitude indicates the latitude that the drone flies to
     */
    public FlightPath(double fromLongitude, double fromLatitude, Double angle, double toLongitude, double toLatitude){
        this.fromLongitude = fromLongitude;
        this.fromLatitude = fromLatitude;
        this.angle = angle;
        this.toLongitude = toLongitude;
        this.toLatitude = toLatitude;
    }

}
