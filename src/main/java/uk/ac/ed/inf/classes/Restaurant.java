package uk.ac.ed.inf.classes;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is used to store information about Restaurants fetched from the server. <br>
 * There are 4 private properties in this class: name(String), longitude(double), latitude(double), and menus(Menu[]).
 */
public class Restaurant {
    /**
     * The private property indicates the name of the current restaurant
     */
    @JsonProperty("name")
    private String name;

    /**
     * The private property indicates the longitude of the current restaurant
     */
    @JsonProperty("longitude")
    private double longitude;

    /**
     * The private property indicates the latitude of the current restaurant
     */
    @JsonProperty("latitude")
    private double latitude;

    /**
     * The private property indicates the menus of the current restaurant
     */
    @JsonProperty("menu")
    private Menu[] menus;

    /**
     * This is the Getter method for the private property name
     * @return the value of private property name
     */
    public String getName(){return this.name;}

    /**
     * This is the Getter method for the private property longitude
     * @return the value of private property longitude
     */
    public double getLongitude(){return this.longitude;}

    /**
     * This is the Getter method for the private property latitude
     * @return the value of private property latitude
     */
    public double getLatitude(){return this.latitude;}

    /**
     * This is the Getter method for the private property menus
     * @return the value of private property menus
     */
    public Menu[] getMenus() {return this.menus;}

}
