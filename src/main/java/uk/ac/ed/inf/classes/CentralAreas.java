package uk.ac.ed.inf.classes;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is used to store information about central area retrieved from the server. <br>
 * A single CentralAreas represents a single point that forms the central area. <br>
 * There are 3 private properties in the CentralAreas class: name(String), longitude(double), and latitude(double)
 */
public class CentralAreas {
    /**
     * This private property name indicates the name of the point that forms the central areas
     */
    @JsonProperty("name")
    private String name;

    /**
     * The private property longitude indicates the longitude of the point
     */
    @JsonProperty("longitude")
    private double longitude;

    /**
     * The private property latitude indicates the latitude of the point
     */
    @JsonProperty("latitude")
    private double latitude;

    /**
     * Getter method for the private property name
     * @return the value of private property name
     */
    public String getName() {return name;}

    /**
     * Getter method for the private property longitude
     * @return the value of private property longitude
     */
    public double getLongitude() {return longitude;}

    /**
     * Getter method for the private property latitude
     * @return the value of private property latitude
     */
    public double getLatitude() {return latitude;}
}
