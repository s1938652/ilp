package uk.ac.ed.inf.classes;

import uk.ac.ed.inf.eunms.DroneDirections;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;
import java.util.*;

/**
 * This class is used to store information about the location of a point. <br>
 * This class has 2 private properties: lng(double) and lat(double) which represent the longitude and the latitude of the current position.<br>
 */
public class LngLat {
    /**
     * This private property is to represent the point's longitude
     */
    private double lng;

    /**
     * This private property is to represent the point's latitude
     */
    private double lat;

    /**
     * This is the LngLat constructor, it will take two arguments and set them to corresponding private properties
     * @param longitude -represents the longitude of the current position
     * @param latitude -represents the latitude of the current position
     */
    public LngLat(double longitude, double latitude){
        this.lng = longitude;
        this.lat = latitude;
    }

    /**
     * Getter method for the private property lng
     * @return the value of private property lng which represents the current point's longitude
     */
    public double getLng() {
        return lng;
    }

    /**
     * Getter method for the private property lat
     * @return the value of private property lat which represents the current point's latitude
     */
    public double getLat() {
        return lat;
    }


    /**
     * This method computes the new location after using the current location taking one of the 16 legal directions or hover(null)
     * @param direction - this is the direction that the drones will take, including hover(null)
     * @return a new LngLat class that represents the new location after taking the move
     */
    public LngLat nextPosition(DroneDirections direction){
        //if the drone chooses to hover
        if (direction == null){
            return new LngLat(this.lng, this.lat);
        }
        //compute the radians and the cos and sin of the angles
        double radians = Math.toRadians(direction.getAngles());
        double cos = Math.cos(radians);
        double sin = Math.sin(radians);

        double new_lng = this.lng+Utils.MAXIMUM_STEP_PER_MOVE *cos;
        double new_lat = this.lat+Utils.MAXIMUM_STEP_PER_MOVE *sin;
        return new LngLat(new_lng,new_lat);
    }

    /**
     * This method calculates the euclidean distance of the current position and the input position.
     * @param point - the given LongLat class represents a position that needs to be calculated
     * @return Pythagorean distance between the current LngLat class and the input LngLat class
     */
    public double distanceTo(LngLat point){
        if(point==null){
            throw new NullPointerException("The given point shouldn't be null.");
        }
        double Lng_diff = this.lng - point.lng;
        double Lat_diff = this.lat - point.lat;
        double distance = Math.sqrt(Lng_diff*Lng_diff+Lat_diff*Lat_diff);
        return distance;
    }

    /**
     * This method judges if the current LngLat class is inside the central area
     * @return true if the drone is in the centralArea
     *         false if it is not in the central area
     */
    public boolean inCentralAreas(){
        int num_vertexes = Client.getCentralAreas_instance().size();
        double[] central_longitudes = new double[num_vertexes+1];
        double[] central_latitudes = new double[num_vertexes+1];
        //Distributes the longitude and latitude of central_area_info to central_longitudes and central_latitudes
        for(int i=0; i<num_vertexes; i++){
            double curr_longitude = Client.getCentralAreas_instance().get(i).getLongitude();
            double curr_latitude = Client.getCentralAreas_instance().get(i).getLatitude();
            central_longitudes[i] = curr_longitude;
            central_latitudes[i] = curr_latitude;
        }
        //add the initial point to make it close
        central_longitudes[num_vertexes] = Client.getCentralAreas_instance().get(0).getLongitude();
        central_latitudes[num_vertexes] = Client.getCentralAreas_instance().get(0).getLatitude();
        return point_in_Polygon(central_longitudes,central_latitudes,this.lng,this.lat);
    }

    /**
     * This method decides whether the current LngLat class is in any of the no-fly zones
     * @return true if the current LngLat class is inside any of the no-fly zones
     *         false if the current LngLat class is not inside any of the no-fly zones
     */
    public boolean inNoFlyZones(){
        int noFlyZones_num = Client.getNoFlyZones_instance().size();
        for (int i=0; i<noFlyZones_num; i++){
            NoFlyZone nf = Client.getNoFlyZones_instance().get(i);
            int num_vertexes = nf.getCoordinates().length;
            double[] curr_noFlyZone_longitudes = new double[num_vertexes];
            double[] curr_noFlyZone_latitudes = new double[num_vertexes];
            //Distributes the longitude and latitude of current no-fly zone to curr_noFlyZone_longitudes and curr_noFlyZone_latitudes
            for (int j=0; j<num_vertexes; j++){
                curr_noFlyZone_longitudes[j] = nf.getCoordinates()[j][0];
                curr_noFlyZone_latitudes[j] = nf.getCoordinates()[j][1];
            }
            boolean in_restricted_area = point_in_Polygon(curr_noFlyZone_longitudes,curr_noFlyZone_latitudes,this.lng,this.lat);
            //return true if the point is in any of the no-fly zone
            if (in_restricted_area){
                return true;
            }
        }
        return false;
    }

    /**
     * This method determines whether the given LngLat class is close to the current LngLat class with respect to the TOLERANCE_DEGREES
     * @param point - the LngLat class which is used to decide if the current LngLat class is closed to
     * @return true if they are closed to each other,
     *         false if they are not closed to each other
     */
    public boolean closeTo(LngLat point){
        double distance = distanceTo(point);
        return distance < Utils.TOLERANCE_DEGREES;
    }

    /**
     * This method decides if the line formed by the current LngLat class and the given LngLat class pass any of the no-fly zones
     * @param point the given LngLat class
     * @return true if the line formed by two LngLat class pass any of the no-fly zones thus violates the rule
     *         false if the line formed by two LngLat class do not pass any of the no-fly zones and didn't violate the rule
     */
    public boolean checkIfViolatesNoFlyZonesRule(LngLat point){
        if(point==null){
            throw new NullPointerException("Parameters in PassesNoFlyZones is null");
        }

        //if there is empty no-fly zones which means every path is legal
        if (Client.getNoFlyZones_instance() == null){
            return false;
        }

        //if one of the LngLat class is in one of the no-fly zones then it must violate the rule
        if (this.inNoFlyZones()|point.inNoFlyZones()){
            return true;
        }
        //traverse all the no-fly zones to see if the line segment formed by the two LngLat class have intersection point with the no-fly zones
        for (int i = 0; i<Client.getNoFlyZones_instance().size(); i++){
            NoFlyZone curr_nfz = Client.getNoFlyZones_instance().get(i);
            for (int j=0; j<curr_nfz.getCoordinates().length-1; j++){
                //convert the edge point to LngLat class
                LngLat coordinate1 = new LngLat(curr_nfz.getCoordinates()[j][0],curr_nfz.getCoordinates()[j][1]);
                LngLat coordinate2 = new LngLat(curr_nfz.getCoordinates()[j+1][0],curr_nfz.getCoordinates()[j+1][1]);
                //find the intersection point of two straight line which are formed by line1(coordinate1, coordinate2) and line2(the current LngLat, the input LngLat class)
                LngLat intersection = findIntersectionPoint(this,point,coordinate1,coordinate2);
                //if two lines don't have intersections, we continue to next pair of edges
                if(intersection==null){
                    continue;
                }
                //if we do have intersections and the intersection point lies on both two lines which means the two line segment have intersections then this must violate the rule we return
                if(intersectionOnLine(coordinate1,coordinate2,intersection)&& intersectionOnLine(this,point,intersection)){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This method decides whether a line compromised by the current LngLat class and the given LngLat class violates the central area’s rule. <br>
     * This function can handle any corner case which means no matter how strange is the shape of central area, this method can find out the correct answer <br>
     * The main idea:
     * Step 1: find the intersection points of the current LngLat class and the given LngLat class, and sort these intersection points by there euclidean distance to the current point.
     * Step 2: delete intersection points that are two close to each other with respect to the Util.Maximum_Error
     * Step 3: find if the inCentralArea property changes when traverse the intersection points.
     *         for example:
     *         if we have 3 intersection points, and they are already sorted, also delete the same points. [point1,point2,point3]
     *         check the start LngLat class's inCentralArea, and computes inCentralArea of the midpoint between point1 and point2,
     *         if this is different from the initial inCentralArea, if the drone is permitted to leave or enter this should be fine, if not this violates the rule.
     *         if the midpoint between point2 and point3 changes again, this must violate the rule.
     * Step 4: return the map(String, Boolean) which has the information of if the move violate the rule and if the drone can leave or enter the central area again
     * @param point the given LngLat class
     * @param canLeaveOrEnter indicates whether the drone is permitted to leave or enter the central area
     * @return Map(String,Boolean) which has exactly two keys: <br><br>
     *         The first key is "ViolatesRule" and this represents if the line violates the centralArea's rule:<br>
     *                  True if this move violates the rule, and False if this move doesn't violate the rules<br>
     *
     *         The second key is "canLeaveOrEnter" and this represents that after this move can the drone enter or leave the central area again<br>
     *                  True if the drone can Leave or enter again, and False if it can't leave or enter again, null if this move already violates the central area's rule
     */
    public Map<String,Boolean> checkIfViolatesCentralAreaRule(LngLat point, boolean canLeaveOrEnter) {
        if (point == null) {
            throw new NullPointerException("Parameters in PassesNoFlyZones is null");
        }
        Map<String, Boolean> results= new HashMap<>();
        String key1 = "ViolatesRule";
        String key2 = "canLeaveOrEnter";

        LngLat end_point = point;
        List<LngLat> intersection_points = new ArrayList<>();

        //finding all the intersection points with each edges of central areas
        for (int i = 0; i<Client.getCentralAreas_instance().size(); i++){
            CentralAreas second_term;
            if(i == Client.getCentralAreas_instance().size()-1){
                second_term = Client.getCentralAreas_instance().get(0);
            }else {
                second_term = Client.getCentralAreas_instance().get(i+1);
            }
            LngLat point1 = new LngLat(Client.getCentralAreas_instance().get(i).getLongitude(),Client.getCentralAreas_instance().get(i).getLatitude());
            LngLat point2 = new LngLat(second_term.getLongitude(), second_term.getLatitude());
            LngLat intersection = findIntersectionPoint(point1,point2,this,point);
            //case when the two lines don't have intersections
            if(intersection==null){
                continue;
            }
            if(intersectionOnLine(point1,point2,intersection)&&intersectionOnLine(this,point,intersection)){
                intersection_points.add(intersection);
            }
        }

        //If we have no intersection points then we are guaranteed with a valid flight path
        if(intersection_points.size()==0){
            results.put(key1,Boolean.FALSE);
            results.put(key2,canLeaveOrEnter);
            return results;
        }
        Boolean start_point_in_cas = this.inCentralAreas();
        Boolean end_point_in_cas = end_point.inCentralAreas();

        //Make the list unique by removing similar intersection points
        for (int i=0; i<intersection_points.size(); i++){
            for(int j = i+1; j<intersection_points.size(); j++){
                if(intersection_points.get(i).distanceTo(intersection_points.get(j))<Utils.MAXIMUM_ERROR){
                    intersection_points.remove(j);
                }
            }
        }

        //Case when we have 1 intersection point
        if(intersection_points.size()==1){
            if(start_point_in_cas&end_point_in_cas){
                results.put(key1,Boolean.FALSE);
                results.put(key2,canLeaveOrEnter);
                return results;
            }
            if(!start_point_in_cas&!end_point_in_cas){
                results.put(key1,Boolean.TRUE);
                results.put(key2,null);
                return results;
            }
            else if (canLeaveOrEnter) {
                results.put(key1, Boolean.FALSE);
                results.put(key2, Boolean.FALSE);
                return results;
            } else {
                results.put(key1, Boolean.TRUE);
                results.put(key2, null);
                return results;
            }
        }

        //Sort the list with respect to its distance to the start part
        Collections.sort(intersection_points, new Comparator<LngLat>() {
            @Override
            public int compare(LngLat o1, LngLat o2) {
                double diff = o1.distanceTo(end_point)-o2.distanceTo(end_point);
                if(diff>0){
                    return -1;
                }
                if(diff==0){
                    return 0;
                }else {
                    return 1;
                }
            }
        });

        Boolean previous_in_cas = start_point_in_cas;
        //Traverse all the points and finds out whether the drone enters and then leaves or leaves then enters
        for(int i = 0; i<intersection_points.size()-1;i++){
            LngLat first = intersection_points.get(i);
            LngLat second = intersection_points.get(i+1);
            LngLat mid = new LngLat((first.getLng()+second.getLng())/2,(first.getLat()+second.getLat())/2);
            //If the current has different inCentralArea value with the previous one which means we are moving in or out
            if( previous_in_cas != mid.inCentralAreas()){
                //drones with the permission to leave or enter should be set to False because it has already left or entered the central areas
                if (canLeaveOrEnter){
                    canLeaveOrEnter = Boolean.FALSE;
                }
                //If the drone is not permitted to leave or enter then we should treat this as invalid
                else {
                    results.put(key1,Boolean.TRUE);
                    results.put(key2,null);
                    return results;
                }
            }
            previous_in_cas = mid.inCentralAreas();
        }
        //if the end points has the different inCentralArea value with the previous
        if(previous_in_cas != end_point_in_cas){
            //If the drone is permitted to leave or enter
            if(canLeaveOrEnter){
                results.put(key1,Boolean.FALSE);
                results.put(key2,Boolean.FALSE);
                return results;
            }
            //If the drone is not permitted to leave or enter then we should treat this as invalid
            results.put(key1,Boolean.TRUE);
            results.put(key2,null);
            return results;
        }
        results.put(key1,Boolean.FALSE);
        results.put(key2,canLeaveOrEnter);
        return results;
    }

    /**
     * This method helps to compute if a given point is inside the polygon or not
     * @param xs this is the array of longitudes which describes the polygon
     * @param ys this is the array of latitudes which describes the polygon
     * @param x this is the current longitude of the drone
     * @param y this is the current latitude of the drone
     * @return True if the point is in the centralArea and False if it isn't
     */
    public static Boolean point_in_Polygon(double[] xs, double[] ys, double x, double y) {
        if ((xs == null)|(ys==null)){
            throw new NullPointerException("The input array shouldn't be null.");
        }
        if (xs.length != ys.length){
            throw new NullPointerException("The xs and ys should have the same length");
        }
        int number_of_inter = 0;
        for (int i=0; i < xs.length-1; i++){
            double x1 = xs[i];
            double y1 = ys[i];
            double x2 = xs[i+1];
            double y2 = ys[i+1];
            //computes the intersection of the line Y=x(a constant line) with the line formed by two adjacent vertex
            double control = (x2*(y-y1) - x1*(y-y2))/(y2-y1);

            //case if the drone stays in an oblique line or the vertex
            if (control == x){
                return Boolean.TRUE;
            }

            //case if the drone stays in the horizontal line formed by two adjacent vertex
            if ((x<x1 != x<x2) && (y==y1 && y==y2)){
                return Boolean.TRUE;
            }

            //case if the drone stays between y1 and y2, and the intersection point is on the right of x
            if ((y<y1 != y<y2) && (x < control)){
                number_of_inter ++;
            }
        }
        if (number_of_inter % 2 == 0){
            return Boolean.FALSE;
        }else {
            return Boolean.TRUE;
        }
    }

    /**
     * This method helps to find the intersection point of straight lines(not line segment) formed by point 1 and point2 with straight line formed by point3 and point4
     * @param point1 the first given point
     * @param point2 the second given point
     * @param point3 the third given point
     * @param point4 the forth given point
     * @return null if the given two straight line don't have any intersection points
     *         LngLat object represents the point of intersection
     */
    private static LngLat findIntersectionPoint(LngLat point1, LngLat point2, LngLat point3, LngLat point4){
        //throw exception for the null case
        if (point1==null|point2==null|point3==null|point4==null){
            throw new NullPointerException();
        }
        //difference of Longitude of two points is 0, which means 0 in the denominator when computing slope we need to discuss this case separately
        //this is when the difference of these two both equal to 0
        if(point1.getLng()-point2.getLng()==0&point3.getLng()-point4.getLng()==0){
            return null;
        }
        //this is when one of the point's difference of longitude is 0
        if(point1.getLng()-point2.getLng()==0){
            double k1 = (point4.getLat()- point3.getLat())/(point4.getLng()-point3.getLng());
            double b1 = point3.getLat()-k1*point3.getLng();
            return new LngLat(point1.getLng(),k1*point1.getLng()+b1);
        }
        if(point3.getLng()-point4.getLng()==0){
            double k2 = (point2.getLat()-point1.getLat())/(point2.getLng()-point1.getLng());
            double b2 = point1.getLat()-k2*point1.getLng();
            return new LngLat(point3.getLng(),k2*point3.getLng()+b2);
        }

        //This is when we calculate the point of intersection in the normal way
        double k1 = (point4.getLat()- point3.getLat())/(point4.getLng()-point3.getLng());
        double k2 = (point2.getLat()-point1.getLat())/(point2.getLng()-point1.getLng());
        //If the slope is equal we shouldn't expect any chance of intersection
        if (k1==k2){
            return null;
        }
        double b1 = point3.getLat()-k1*point3.getLng();
        double b2 = point1.getLat()-k2*point1.getLng();
        double intersection_lng = (b2-b1)/(k1-k2);
        double intersection_lat = k1*intersection_lng+b1;
        LngLat intersection = new LngLat(intersection_lng,intersection_lat);
        return intersection;
    }

    /**
     * This method is used to check if a given point is on the line segment
     * @param point1 the first point which forms the line segment
     * @param point2 the second point which forms the line segment
     * @param given_point the point that needs to be checked
     * @return true if the given point is on the line segment formed by point1 and point2
     *         false if the given point isn't on the line segment formed by point1 and point2
     */
    private static boolean intersectionOnLine(LngLat point1, LngLat point2, LngLat given_point) {
        if (Math.min(point1.getLng(), point2.getLng()) <= given_point.getLng() && given_point.getLng() <= Math.max(point1.getLng(), point2.getLng()) && Math.min(point1.getLat(), point2.getLat()) <= given_point.getLat() && given_point.getLat() <= Math.max(point1.getLat(), point2.getLat())) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean intersects(LngLat l1, LngLat l2, LngLat l3, LngLat l4){
        LngLat intersection = findIntersectionPoint(l1,l2,l3,l4);
        if(intersection==null){
            return false;
        }
        if(intersectionOnLine(l1,l2,intersection)&&intersectionOnLine(l3,l4,intersection)){
            return true;
        }
        return false;
    }

    /**
     * This is an override which determines if the current LngLat class is equal to another given LngLat class with respect to their lng and lat
     * @param o the given LngLat object
     * @return true if these two LngLat classes have the same lng and lat<br>
     *         false if these two LngLat classes have the different lng and lat
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LngLat lngLat = (LngLat) o;
        return Double.compare(lngLat.lng, lng) == 0 && Double.compare(lngLat.lat, lat) == 0;
    }

    /**
     * This is an override which determines the hashcode of the current LngLat class
     * @return the hashcode of the current LngLat class
     */
    @Override
    public int hashCode() {
        return Objects.hash(lng, lat);
    }


}
