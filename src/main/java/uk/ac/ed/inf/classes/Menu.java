package uk.ac.ed.inf.classes;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is used to store information about menu retrieved from the server. <br>
 * There are 2 private properties in this class: name(String), and priceInPence(int).
 */
public class Menu {
    /**
     * This private property indicates the name of the pizza
     */
    @JsonProperty("name")
    private String name;

    /**
     * This private property indicates the price of this pizza
     */
    @JsonProperty("priceInPence")
    private int priceInPence;

    /**
     * Getter method for private property name
     * @return the value of private property name
     */
    public String getName() {return name;}

    /**
     * Getter method for private property priceInPence
     * @return the value of private property priceInPence
     */
    public int getPriceInPence() {
        return priceInPence;
    }
}
