package uk.ac.ed.inf.eunms;

/**
 * The DroneDirections class is an enum class which represents the 16 legal directions that the drone can take,
 * It also has a private variable: angles(double), and the angle can be accessed by the getter function.
 * Having this enum will limit the type of input to a function and simplifying the process of determining whether a parameter is valid.
 * @author Jiacheng Cui
 */
public enum DroneDirections {
    //the 16 legal directions that a drone can take
    /**
     * E Represents the direction East and has the angle value 0
     */
    E(0),

    /**
     * ENE Represents the direction East North East and has the angle value 22.5
     */
    ENE(22.5),

    /**
     * NE Represents the direction North East and has the angle value 45
     */
    NE(45),

    /**
     * NNE Represents the direction North North East and has the angle value 67.5
     */
    NNE(67.5),

    /**
     * N Represents the direction North and has the angle value 90
     */
    N(90),

    /**
     * NNW Represents the direction North North West and has the angle value 112.5
     */
    NNW(112.5),

    /**
     * NW Represents the direction North West and has the angle value 135
     */
    NW(135),

    /**
     * WNW Represents the direction West North West and has the angle value 157.5
     */
    WNW(157.5),

    /**
     * W Represents the direction West and has the angle value 180
     */
    W(180),

    /**
     * WSW Represents the direction West South West and has the angle value 202.5
     */
    WSW(202.5),

    /**
     * SW Represents the direction South West and has the angle value 225
     */
    SW(225),

    /**
     * SSW Represents the direction South South West and has the angle value 247.5
     */
    SSW(247.5),

    /**
     * S Represents the direction South and has the angle value 270
     */
    S(270),

    /**
     * SSE Represents the direction South South East and has the angle value 292.5
     */
    SSE(292.5),

    /**
     * SE Represents the direction South East and has the angle value 315
     */
    SE(315),

    /**
     * ESE Represents the direction East South East and has the angle value 337.5
     */
    ESE(337.5);

    //the private property angles represents the numerical value of a direction
    private double angles;

    /**
     * This function is the constructor of the DroneDirections class
     * and will set this numerical value to the angles property
     * @param angles the numerical angle value
     */
    DroneDirections(double angles){this.angles = angles;}

    /**
     * This function is the getter method for the private property angles
     * @return the value of private property angles
     */
    public Double getAngles(){return angles;}


}
