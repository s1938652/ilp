package uk.ac.ed.inf.eunms;

/**
 * The OrderOutcomes class is an enum class which represents the 10 possible outcomes for an order,
 * Having this enum will make the implementation of checking the order’s outcome clear.
 * @author Jiacheng Cui
 */
public enum OrderOutcomes {
    //10 possible outcomes for an order
    /**
     * Delivered means an order is Delivered
     */
    Delivered(),

    /**
     * ValidButNotDelivered means an order is valid but still not delivered to costumers
     */
    ValidButNotDelivered(),

    /**
     * InvalidCardNumber means an order has the wrong card number
     */
    InvalidCardNumber(),

    /**
     * InvalidExpiryDate means the card to purchase this order is expired
     */
    InvalidExpiryDate(),

    /**
     * InvalidCvv means the cvv of the card which used to purchase this order is not valid
     */
    InvalidCvv(),

    /**
     * InvalidTotal means that the total cost of this order is not equal to the real cost
     */
    InvalidTotal(),

    /**
     * InvalidPizzaNotDefined means that some of ordered pizzas is not in any of the active restaurant's menu
     */
    InvalidPizzaNotDefined(),

    /**
     * InvalidPizzaCount means that the number of ordered pizza is not valid
     */
    InvalidPizzaCount(),

    /**
     * InvalidPizzaCombinationMultipleSuppliers means that the ordered pizzas do not come from the same restaurant
     */
    InvalidPizzaCombinationMultipleSuppliers(),

    /**
     * Invalid means the order is invalid because of no customer's name, the ordered date is different from the current date or other circumstances
     * that do not belong to any other invalid situations
     */
    Invalid();

}
