package uk.ac.ed.inf;

import uk.ac.ed.inf.eunms.OrderOutcomes;
import uk.ac.ed.inf.utils.Client;
import uk.ac.ed.inf.utils.Utils;
import uk.ac.ed.inf.utils.Utils_FlightPath;
import uk.ac.ed.inf.utils.Utils_Order;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * The App class is designated as the main entry of the whole project, and it is used to specify the project's main flow and control the order in which different functions are called. <br>
 * <b>Main Procedure:</b> <br>
 *  Step 1 check if the input arguments is valid or not if not end the program<br>
 *  Step 2 Use the Client class to initialize the orders, no-fly zones, restaurants, and central areas this will end the program if the Url base we offer is incorrect<br>
 *  Step 3 Use the Util_Order class to check the orders fetched from the server and assign each order with one OrderOutcome<br>
 *  Step 4 Use the Client class to check the fetched information from the server and will end the program and write 3 JSON and GeoJson files if we have 0 orders, 0 central ares, or 0 restaurants<br>
 *  Step 5 Use the Utils_FlightPath class to compute the flight paths to deliver the valid order.<br>
 *  Step 6 Use Util class to Write the 3 JSON and GeoJson file<br>
 * @author Jiacheng Cui
 */
public class App {

    /**
     * This function is used to check if a given date is in the form of "yyyy-MM-dd"<br>
     * If the given input is not in the correct form, it will throw ParseException<br>
     * If we catch this exception, we will end the program
     * @param date a String that represents a specific date that needs to be checked
     */
    private static void checkDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setLenient(false);
        try {
            df.parse(date);
        } catch (ParseException e) {
            System.err.println("The input date is not in the form yyyy-MM-dd, ending the program.....");
            System.exit(0);
        }
    }

    /**
     * This function is used to check if a given url is in a valid URL
     * and will end the program if the input url_str is invalid
     * @param url_str a String that represents the base url
     * @return URL if the input url_str is valid
     */
    private static URL checkUrl(String url_str){
        URL base_url = null;
        try {
            base_url = new URL(url_str);
        } catch (MalformedURLException e) {
            System.err.println("The given base url is incorrect, ending the program.....");
            System.exit(0);
        }
        return base_url;
    }

    /**
     * This is the main entry of the whole project
     * @param args An array that contains three useful information<br>
     *             The first is the delivery's date<br>
     *             The second is the Url of the server<br>
     *             The third is the random variable<br>
     */
    public static void main(String [] args){
        //check if we have 3 arguments
        if(args.length!=3){
            System.err.println("Please give three arguments in the sequence of delivery date, base url, and random variable");
            System.exit(0);
        }

        long time = System.currentTimeMillis();
        /*
         * Step 1, check if the input arguments is valid or not
         * if not end the program
         */
        String date = args[0];
        String url_str = args[1];
        //check if the date and url_str is valid
        checkDate(date);
        System.out.println("The chosen deliver date is: " + date);
        URL base_url = checkUrl(url_str);
        //set the url and date to Utils
        Utils.setBaseURL(base_url);
        Utils.setDelivery_date(date);
        Utils.setStartPoint(Utils.APPLETON_TOWER);
        System.out.println("We have finished step 1: check if the input arguments is valid or not");

        /*
         * Step 2 Use the Client class to initialize the orders, no-fly zones, restaurants, and central areas
         * this will end the program if the Url base we offer is incorrect
         */
        Client.InitializeInformation();
        System.out.println("We have finished step 2: initialize the orders, no-fly zones, restaurants, and central areas");

        /*
         * Step 3 Use the Util_Order class to check the orders fetched from the server
         * and assign each order with one OrderOutcome
         */
        Utils_Order.checkOrders();
        System.out.println("We have finished step 3: check the orders fetched from the server and assign each order with one OrderOutcome");

        /*
         * Step 4 Use the Client class to check the fetched information from the server
         * and will end the program and write 3 JSON and GeoJson files if we have 0 orders, 0 central ares, or 0 restaurants
         */
        Client.checkClientInformation();
        System.out.println("We have finished step 4: check if we are have 0 size orders, 0 size central areas, or 0 size restaurants in client");


        /*
         * Step 5 Use the Utils_FlightPath class to compute the flight paths to deliver the valid orders
         */
        Utils_FlightPath.computeFlightPaths();
        System.out.println("We have finished step 5: compute the flight paths to deliver orders");

        /*
         * Step 6 Use Util class to Write the 3 JSON and GeoJson file
         */
        Utils.writeJsonFile("./resultfiles/");
        System.out.println("We have finished step 6: write the 3 JSON and GeoJson files");
        System.out.println("\nWe delivered "+Utils_Order.checkNumberOfOrdersWithSatisfiedOC(OrderOutcomes.Delivered)+" orders"+" We have "+ Client.getOrders_instance().size()+ " orders");

        //compute the time we used for the whole program
        long end_time = System.currentTimeMillis();
        System.out.println("We used "+(end_time-time)+" ms to finish the program");
    }
}